<?php

require_once 'cronjobConfig.php';

$date = date("Y-m-d");
$baseurl = "http://p546259.mittwaldserver.info/owncsv/";
$year = date("Y");
$month = date("m");
$char_slash = "/";
$basefilename = "2-COVIDCronjob-19-";
$extension = ".csv";
$date_before = date( 'Y-m-d', strtotime( $date . ' -1 day' ) );
$month_before = date( 'm', strtotime( $date . ' -1 day' ) );
$year_before = date( 'Y', strtotime( $date . ' -1 day' ) );

$csvName = $baseurl . $basefilename.$date.$extension;


$row = 1;
$rowDayBefore = 1;
$arrayCountriesDaybefore = array();

//for getting data before today


//for getting today's data

$confirmed =0;
$deaths    = 0;
$recovered =0;


$arrayCountries = array();
if (($handle = fopen($csvName, "r")) !== FALSE) {
	
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
    if($row == 1 ){
      if($data[0] != 'Country'){
        die();	
      }
    }

    if($row != 1) {
      	$arrayCountry = array();
      	$arrayCountry['countryname']= $data[0];
      	$arrayCountry['confirmed']= $data[1];
        $arrayCountry['deaths']= $data[2];
        $arrayCountry['recovered']= $data[3];
      	$arrayCountry['latitude']= $data[4];
      	$arrayCountry['longitude']= $data[5];
		$arrayCountry['countrycode']= $data[6];
		$radius = 0;
		$radius = ((sqrt($arrayCountry['confirmed'] / pi()))*5000)/2;
		$dataArray =array('countryName'=>$arrayCountry['countryname'],'latitude'=>$arrayCountry['latitude'],'longitude'=>$arrayCountry['longitude'],'radius'=>$radius,'countryCode'=>$static_countries[$arrayCountry['countryname']]['cn_iso_2']);
		$countryDetails [] = $dataArray;


    }
		$row++;
		
    }
	fclose($handle);
	
    if(!empty($countryDetails)){
        $finaldata=json_encode(array(
            'status' => 200, // success or not?
            'message' =>'success',
			'mapdetails' => $countryDetails
			
            ));
            header('Content-type: application/json');
            print_r( $finaldata);
            return $finaldata;
    }
    header('Content-type: application/json');
    return json_encode(array(
        'status' => 200, // success or not?
        'message' =>'failed',
		'mapdetails' => [],
		
        ));
	

		
}



?>