<?php


//error_reporting(E_ALL);
//ini_set("display_errors",1);
date_default_timezone_set('Europe/Berlin'); 
ini_set("allow_url_fopen", 1);


//static countries array 

$static_countries = array(
  "Andorra" => array('cn_iso_2' => 'AD','cn_short_de' => 'Andorra','size'=> '77000'),
  "United Arab Emirates" => array('cn_iso_2' => 'AE','cn_short_de' => 'Vereinigte Arabische Emirate','size'=> '9992083'),
  "Afghanistan" => array('cn_iso_2' => 'AF','cn_short_de' => 'Afghanistan','size'=> '36643815'),
  "Antigua and Barbuda" => array('cn_iso_2' => 'AG','cn_short_de' => 'Antigua und Barbuda','size'=> '98179'),
  "Anguilla" => array('cn_iso_2' => 'AI','cn_short_de' => 'Anguilla','size'=> '18090'),
  "Albania" => array('cn_iso_2' => 'AL','cn_short_de' => 'Albanien','size'=> '3074579'),
  "Armenia" => array('cn_iso_2' => 'AM','cn_short_de' => 'Armenien','size'=> '3021324'),
  
  "Angola" => array('cn_iso_2' => 'AO','cn_short_de' => 'Angola','size'=> '32522339'),
  
  "Argentina" => array('cn_iso_2' => 'AR','cn_short_de' => 'Argentinien','size'=> '45479118'),
  
  "Austria" => array('cn_iso_2' => 'AT','cn_short_de' => 'Österreich','size'=> '8859449'),
  "Australia" => array('cn_iso_2' => 'AU','cn_short_de' => 'Australien','size'=> '25466459'),
  "Aruba" => array('cn_iso_2' => 'AW','cn_short_de' => 'Aruba','size'=> '119428'),
  "Azerbaijan" => array('cn_iso_2' => 'AZ','cn_short_de' => 'Aserbaidschan','size'=> '10205810'),
  "Bosnia and Herzegovina" => array('cn_iso_2' => 'BA','cn_short_de' => 'Bosnien und Herzegowina','size'=> '3835586'),
  "Barbados" => array('cn_iso_2' => 'BB','cn_short_de' => 'Barbados','size'=> '294560'),
  "Bangladesh" => array('cn_iso_2' => 'BD','cn_short_de' => 'Bangladesch','size'=> '162650853'),
  "Belgium" => array('cn_iso_2' => 'BE','cn_short_de' => 'Belgien','size'=> '11720716'),
  "Burkina Faso" => array('cn_iso_2' => 'BF','cn_short_de' => 'Burkina Faso','size'=> '20835401'),
  "Bulgaria" => array('cn_iso_2' => 'BG','cn_short_de' => 'Bulgarien','size'=> '6966899'),
  "Bahrain" => array('cn_iso_2' => 'BH','cn_short_de' => 'Bahrain','size'=> '1505003'),
  "Burundi" => array('cn_iso_2' => 'BI','cn_short_de' => 'Burundi','size'=> '11865821'),
  "Benin" => array('cn_iso_2' => 'BJ','cn_short_de' => 'Benin','size'=> '12864634'),
  "Bermuda" => array('cn_iso_2' => 'BM','cn_short_de' => 'Bermuda','size'=> '71750'),
  "Brunei" => array('cn_iso_2' => 'BN','cn_short_de' => 'Brunei','size'=> '464478'),
  "Bolivia" => array('cn_iso_2' => 'BO','cn_short_de' => 'Bolivien','size'=> '11639909'),
  "Brazil" => array('cn_iso_2' => 'BR','cn_short_de' => 'Brasilien','size'=> '211715973'),
  "The Bahamas" => array('cn_iso_2' => 'BS','cn_short_de' => 'Bahamas','size'=> '337721'),
  "Bhutan" => array('cn_iso_2' => 'BT','cn_short_de' => 'Bhutan','size'=> '782318'),
  
  "Botswana" => array('cn_iso_2' => 'BW','cn_short_de' => 'Botswana','size'=> '2317233'),
  "Belarus" => array('cn_iso_2' => 'BY','cn_short_de' => 'Weißrussland','size'=> '9477918'),
  "Belize" => array('cn_iso_2' => 'BZ','cn_short_de' => 'Belize','size'=> '374681'),
  "Canada" => array('cn_iso_2' => 'CA','cn_short_de' => 'Kanada','size'=> '37694085'),
  
  "Congo" => array('cn_iso_2' => 'CD','cn_short_de' => 'Kongo','size'=> '5293070'),
  "Central African Republic" => array('cn_iso_2' => 'CF','cn_short_de' => 'Zentral Afrika','size'=> '5990855'),
  "Congo-Brazzaville" => array('cn_iso_2' => 'CG','cn_short_de' => 'Kongo','size'=> '5293070'),
  "Switzerland" => array('cn_iso_2' => 'CH','cn_short_de' => 'Schweiz','size'=> '8403994'),
  "Côte d’Ivoire" => array('cn_iso_2' => 'CI','cn_short_de' => 'Elfenbeinküste','size'=> '27481086'),
  
  "Chile" => array('cn_iso_2' => 'CL','cn_short_de' => 'Chile','size'=> '18186770'),
  "Cameroon" => array('cn_iso_2' => 'CM','cn_short_de' => 'Kamerun','size'=> '27744989'),
  "Mainland China" => array('cn_iso_2' => 'CN','cn_short_de' => 'China','size'=> '1394015977'),
  "Colombia" => array('cn_iso_2' => 'CO','cn_short_de' => 'Kolumbien','size'=> '49084841'),
  "Costa Rica" => array('cn_iso_2' => 'CR','cn_short_de' => 'Costa Rica','size'=> '5097988'),
  "Cuba" => array('cn_iso_2' => 'CU','cn_short_de' => 'Kuba','size'=> '11059062'),
  "Cape Verde" => array('cn_iso_2' => 'CV','cn_short_de' => 'Kap Verde','size'=> '583255'),
  
  "Cyprus" => array('cn_iso_2' => 'CY','cn_short_de' => 'Zypern','size'=> '1266676'),
  "Czech Republic" => array('cn_iso_2' => 'CZ','cn_short_de' => 'Tschechien','size'=> '10702498'),
  "Germany" => array('cn_iso_2' => 'DE','cn_short_de' => 'Deutschland','size'=> '80159662'),
  "Djibouti" => array('cn_iso_2' => 'DJ','cn_short_de' => 'Dschibuti','size'=> '921804'),
  "Denmark" => array('cn_iso_2' => 'DK','cn_short_de' => 'Dänemark','size'=> '5869410'),
  
  "Dominican Republic" => array('cn_iso_2' => 'DO','cn_short_de' => 'Dominikanische Republik','size'=> '10499707'),
  "Algeria" => array('cn_iso_2' => 'DZ','cn_short_de' => 'Algerien','size'=> '42972878'),
  "Ecuador" => array('cn_iso_2' => 'EC','cn_short_de' => 'Ecuador','size'=> '16904867'),
  "Estonia" => array('cn_iso_2' => 'EE','cn_short_de' => 'Estland','size'=> '1228624'),
  "Egypt" => array('cn_iso_2' => 'EG','cn_short_de' => 'Ägypten','size'=> '104124440'),
  
  "Eritrea" => array('cn_iso_2' => 'ER','cn_short_de' => 'Eritrea','size'=> '6081196'),
  "Spain" => array('cn_iso_2' => 'ES','cn_short_de' => 'Spanien','size'=> '50015792'),
  "Ethiopia" => array('cn_iso_2' => 'ET','cn_short_de' => 'Äthiopien','size'=> '108113150'),
  "Finland" => array('cn_iso_2' => 'FI','cn_short_de' => 'Finnland','size'=> '5571665'),
  "Fiji" => array('cn_iso_2' => 'FJ','cn_short_de' => 'Fidschi','size'=> '935974'),
  "Falkland Islands" => array('cn_iso_2' => 'FK','cn_short_de' => 'Falklandinseln','size'=> '3198'),

  "France" => array('cn_iso_2' => 'FR','cn_short_de' => 'Frankreich','size'=> '67848156'),
  "Gabon" => array('cn_iso_2' => 'GA','cn_short_de' => 'Gabun','size'=> '2230908'),
  "UK" => array('cn_iso_2' => 'GB','cn_short_de' => 'Vereinigtes Königreich','size'=> '65761117'),
  "Grenada" => array('cn_iso_2' => 'GD','cn_short_de' => 'Grenada','size'=> '113094'),
  "Georgia" => array('cn_iso_2' => 'GE','cn_short_de' => 'Georgien','size'=> '3997000'),
  
  "Ghana" => array('cn_iso_2' => 'GH','cn_short_de' => 'Ghana','size'=> '29340248'),
  "Gibraltar" => array('cn_iso_2' => 'GI','cn_short_de' => 'Gibraltar','size'=> '29581'),
  "Greenland" => array('cn_iso_2' => 'GL','cn_short_de' => 'Grönland','size'=> '57616'),
  "Gambia" => array('cn_iso_2' => 'GM','cn_short_de' => 'Gambia','size'=> '2173999'),

  "Equatorial Guinea" => array('cn_iso_2' => 'GQ','cn_short_de' => 'Äquatorialguinea','size'=> '836178'),
  "Greece" => array('cn_iso_2' => 'GR','cn_short_de' => 'Griechenland','size'=> '10607051'),
  
  "Guatemala" => array('cn_iso_2' => 'GT','cn_short_de' => 'Guatemala','size'=> '17153288'),
  
  "Guinea-Bissau" => array('cn_iso_2' => 'GW','cn_short_de' => 'Guinea-Bissau','size'=> '2065173'),
  "Guyana" => array('cn_iso_2' => 'GY','cn_short_de' => 'Guyana','size'=> '750204'),
  "Hong Kong" => array('cn_iso_2' => 'HK','cn_short_de' => 'Hong Kong','size'=> '7249907'),
  "Honduras" => array('cn_iso_2' => 'HN','cn_short_de' => 'Honduras','size'=> '9235340'),
  "Croatia" => array('cn_iso_2' => 'HR','cn_short_de' => 'Kroatien','size'=> '4227746'),
  "Haiti" => array('cn_iso_2' => 'HT','cn_short_de' => 'Haiti','size'=> '11067777'),
  "Hungary" => array('cn_iso_2' => 'HU','cn_short_de' => 'Ungarn','size'=> '9771827'),
  "Indonesia" => array('cn_iso_2' => 'ID','cn_short_de' => 'Indonesien','size'=> '267026366'),
  "Ireland" => array('cn_iso_2' => 'IE','cn_short_de' => 'Irland','size'=> '5176569'),
  "Israel" => array('cn_iso_2' => 'IL','cn_short_de' => 'Israel','size'=> '8675475'),
  "India" => array('cn_iso_2' => 'IN','cn_short_de' => 'Indien','size'=> '1326093247'),
  
  "Iraq" => array('cn_iso_2' => 'IQ','cn_short_de' => 'Irak','size'=> '38872655'),
  "Iran" => array('cn_iso_2' => 'IR','cn_short_de' => 'Iran','size'=> '84923314'),
  "Iceland" => array('cn_iso_2' => 'IS','cn_short_de' => 'Island','size'=> '350734'),
  "Italy" => array('cn_iso_2' => 'IT','cn_short_de' => 'Italien','size'=> '62402659'),
  "Jamaica" => array('cn_iso_2' => 'JM','cn_short_de' => 'Jamaika','size'=> '2808570'),
  "Jordan" => array('cn_iso_2' => 'JO','cn_short_de' => 'Jordanien','size'=> '10820644'),
  "Japan" => array('cn_iso_2' => 'JP','cn_short_de' => 'Japan','size'=> '125507472'),
  "Kenya" => array('cn_iso_2' => 'KE','cn_short_de' => 'Kenia','size'=> '53527936'),
  "Kyrgyzstan" => array('cn_iso_2' => 'KG','cn_short_de' => 'Kirgisistan','size'=> '5964897'),
  "Cambodia" => array('cn_iso_2' => 'KH','cn_short_de' => 'Kambodscha','size'=> '16926984'),

  "St. Kitts and Nevis" => array('cn_iso_2' => 'KN','cn_short_de' => 'St. Kitts und Nevis','size'=> '55345'),
  
  "South Korea" => array('cn_iso_2' => 'KR','cn_short_de' => 'Südkorea','size'=> '51835110'),
  "Kuwait" => array('cn_iso_2' => 'KW','cn_short_de' => 'Kuwait','size'=> '2993706'),
  "Cayman Islands" => array('cn_iso_2' => 'KY','cn_short_de' => 'Kaimaninseln','size'=> '61944'),
  "Kazakhstan" => array('cn_iso_2' => 'KZ','cn_short_de' => 'Kasachstan','size'=> '19091949'),
  "Laos" => array('cn_iso_2' => 'LA','cn_short_de' => 'Laos','size'=> '7447396'),
  "Lebanon" => array('cn_iso_2' => 'LB','cn_short_de' => 'Libanon','size'=> '5469612'),
  "Saint Lucia" => array('cn_iso_2' => 'LC','cn_short_de' => 'St. Lucia','size'=> '166487'),
  "Liechtenstein" => array('cn_iso_2' => 'LI','cn_short_de' => 'Liechtenstein','size'=> '39137'),
  "Sri Lanka" => array('cn_iso_2' => 'LK','cn_short_de' => 'Sri Lanka','size'=> '22889201'),
  "Liberia" => array('cn_iso_2' => 'LR','cn_short_de' => 'Liberia','size'=> '5073296'),
  
  "Lithuania" => array('cn_iso_2' => 'LT','cn_short_de' => 'Litauen','size'=> '2731464'),
  "Luxembourg" => array('cn_iso_2' => 'LU','cn_short_de' => 'Luxemburg','size'=> '628381'),
  "Latvia" => array('cn_iso_2' => 'LV','cn_short_de' => 'Lettland','size'=> '1881232'),
  "Libya" => array('cn_iso_2' => 'LY','cn_short_de' => 'Libyen','size'=> '6679000'),
  "Morocco" => array('cn_iso_2' => 'MA','cn_short_de' => 'Marokko','size'=> '35561654'),
  "Monaco" => array('cn_iso_2' => 'MC','cn_short_de' => 'Monaco','size'=> '39000'),
  "Moldova" => array('cn_iso_2' => 'MD','cn_short_de' => 'Moldawien','size'=> '3364496'),
  "Madagascar" => array('cn_iso_2' => 'MG','cn_short_de' => 'Madagaskar','size'=> '26955737'),
  
  "North Macedonia" => array('cn_iso_2' => 'MK','cn_short_de' => 'Nordmazedonien','size'=> '2125971'),
  "Mali" => array('cn_iso_2' => 'ML','cn_short_de' => 'Mali','size'=> '19553397'),
  "Myanmar" => array('cn_iso_2' => 'MM','cn_short_de' => 'Myanmar','size'=> '56590071'),
  "Mongolia" => array('cn_iso_2' => 'MN','cn_short_de' => 'Mongolei','size'=> '3168026'),
  "Macau" => array('cn_iso_2' => 'MO','cn_short_de' => 'Macau','size'=> '614458'),
  
  
  "Mauritania" => array('cn_iso_2' => 'MR','cn_short_de' => 'Mauretanien','size'=> '4005475'),
  "Montserrat" => array('cn_iso_2' => 'MS','cn_short_de' => 'Montserrat','size'=> '5373'),
  "Malta" => array('cn_iso_2' => 'MT','cn_short_de' => 'Malta','size'=> '457267'),
  "Mauritius" => array('cn_iso_2' => 'MU','cn_short_de' => 'Mauritius','size'=> '1379365'),
  "Maldives" => array('cn_iso_2' => 'MV','cn_short_de' => 'Malediven','size'=> '391904'),
  "Malawi" => array('cn_iso_2' => 'MW','cn_short_de' => 'Malawi','size'=> '21196629'),
  "Mexico" => array('cn_iso_2' => 'MX','cn_short_de' => 'Mexiko','size'=> '128649565'),
  "Malaysia" => array('cn_iso_2' => 'MY','cn_short_de' => 'Malaysia','size'=> '32652083'),
  "Mozambique" => array('cn_iso_2' => 'MZ','cn_short_de' => 'Mosambik','size'=> '30098197'),
  "Namibia" => array('cn_iso_2' => 'NA','cn_short_de' => 'Namibia','size'=> '2630073'),
  "New Caledonia" => array('cn_iso_2' => 'NC','cn_short_de' => 'Neukaledonien','size'=> '290009'),
  "Niger" => array('cn_iso_2' => 'NE','cn_short_de' => 'Niger','size'=> '22772361'),
  
  "Nigeria" => array('cn_iso_2' => 'NG','cn_short_de' => 'Nigeria','size'=> '214028302'),
  "Nicaragua" => array('cn_iso_2' => 'NI','cn_short_de' => 'Nicaragua','size'=> '6203441'),
  "Netherlands" => array('cn_iso_2' => 'NL','cn_short_de' => 'Niederlande','size'=> '17280397'),
  "Norway" => array('cn_iso_2' => 'NO','cn_short_de' => 'Norwegen','size'=> '5467439'),
  "Nepal" => array('cn_iso_2' => 'NP','cn_short_de' => 'Nepal','size'=> '30327877'),

  "New Zealand" => array('cn_iso_2' => 'NZ','cn_short_de' => 'Neuseeland','size'=> '4925477'),
  "Oman" => array('cn_iso_2' => 'OM','cn_short_de' => 'Oman','size'=> '4664844'),
  "Panama" => array('cn_iso_2' => 'PA','cn_short_de' => 'Panama','size'=> '3894082'),
  "Peru" => array('cn_iso_2' => 'PE','cn_short_de' => 'Peru','size'=> '31914989'),
  "French Polynesia" => array('cn_iso_2' => 'PF','cn_short_de' => 'Französisch-Polynesien','size'=> '295121'),
  "Papua New Guinea" => array('cn_iso_2' => 'PG','cn_short_de' => 'Papua-Neuguinea','size'=> '7259456'),
  "Philippines" => array('cn_iso_2' => 'PH','cn_short_de' => 'Philippinen','size'=> '109180815'),
  "Pakistan" => array('cn_iso_2' => 'PK','cn_short_de' => 'Pakistan','size'=> '233500636'),
  "Poland" => array('cn_iso_2' => 'PL','cn_short_de' => 'Polen','size'=> '38282325'),

  "Portugal" => array('cn_iso_2' => 'PT','cn_short_de' => 'Portugal','size'=> '10302674'),
  
  "Paraguay" => array('cn_iso_2' => 'PY','cn_short_de' => 'Paraguay','size'=> '7191685'),
  "Qatar" => array('cn_iso_2' => 'QA','cn_short_de' => 'Katar','size'=> '2444174'),
  
  "Romania" => array('cn_iso_2' => 'RO','cn_short_de' => 'Rumänien','size'=> '21302893'),
  "Russia" => array('cn_iso_2' => 'RU','cn_short_de' => 'Russland','size'=> '141722205'),
  "Rwanda" => array('cn_iso_2' => 'RW','cn_short_de' => 'Ruanda','size'=> '12712431'),
  "Saudi Arabia" => array('cn_iso_2' => 'SA','cn_short_de' => 'Saudi-Arabien','size'=> '34173498'),
  
  "Seychelles" => array('cn_iso_2' => 'SC','cn_short_de' => 'Seychellen','size'=> '95981'),
  "Sudan" => array('cn_iso_2' => 'SD','cn_short_de' => 'Sudan','size'=> '45561556'),
  "Sweden" => array('cn_iso_2' => 'SE','cn_short_de' => 'Schweden','size'=> '10202491'),
  "Singapore" => array('cn_iso_2' => 'SG','cn_short_de' => 'Singapur','size'=> '6209660'),
  
  "Slovenia" => array('cn_iso_2' => 'SI','cn_short_de' => 'Slowenien','size'=> '2102678'),
  
  "Slovakia" => array('cn_iso_2' => 'SK','cn_short_de' => 'Slowakei','size'=> '5440602'),
  "Sierra Leone" => array('cn_iso_2' => 'SL','cn_short_de' => 'Sierra Leone','size'=> '6624933'),
  "San Marino" => array('cn_iso_2' => 'SM','cn_short_de' => 'San Marino','size'=> '34232'),
  "Senegal" => array('cn_iso_2' => 'SN','cn_short_de' => 'Senegal','size'=> '15736368'),
  "Somalia" => array('cn_iso_2' => 'SO','cn_short_de' => 'Somalia','size'=> '11757124'),
  "Suriname" => array('cn_iso_2' => 'SR','cn_short_de' => 'Suriname','size'=> '609569'),
  
  "El Salvador" => array('cn_iso_2' => 'SV','cn_short_de' => 'El Salvador','size'=> '6481102'),
  "Syria" => array('cn_iso_2' => 'SY','cn_short_de' => 'Syrien','size'=> '19398448'),
  
  "Turks and Caicos Islands" => array('cn_iso_2' => 'TC','cn_short_de' => 'Turks- und Caicosinseln','size'=> '55926'),
  "Chad" => array('cn_iso_2' => 'TD','cn_short_de' => 'Tschad','size'=> '16877357'),
  "Togo" => array('cn_iso_2' => 'TG','cn_short_de' => 'Togo','size'=> '8608444'),
  "Thailand" => array('cn_iso_2' => 'TH','cn_short_de' => 'Thailand','size'=> '68977400'),

  "Tunisia" => array('cn_iso_2' => 'TN','cn_short_de' => 'Tunesien','size'=> '11721177'),
  
  "Timor-Leste" => array('cn_iso_2' => 'TL','cn_short_de' => 'Osttimor','size'=> '1383723'),
  "Turkey" => array('cn_iso_2' => 'TR','cn_short_de' => 'Türkei','size'=> '82017514'),
  "Trinidad and Tobago" => array('cn_iso_2' => 'TT','cn_short_de' => 'Trinidad und Tobago','size'=> '1208789'),
  
  "Taiwan" => array('cn_iso_2' => 'TW','cn_short_de' => 'Taiwan','size'=> '23603049'),
  "Tanzania" => array('cn_iso_2' => 'TZ','cn_short_de' => 'Tansania','size'=> '58552845'),
  "Ukraine" => array('cn_iso_2' => 'UA','cn_short_de' => 'Ukraine','size'=> '43922939'),
  "Uganda" => array('cn_iso_2' => 'UG','cn_short_de' => 'Uganda','size'=> '43252966'),
  
  "US" => array('cn_iso_2' => 'US','cn_short_de' => 'Vereinigte Staaten','size'=> '332639102'),
  "Uruguay" => array('cn_iso_2' => 'UY','cn_short_de' => 'Uruguay','size'=> '3387605'),
  "Uzbekistan" => array('cn_iso_2' => 'UZ','cn_short_de' => 'Usbekistan','size'=> '30565411'),
  "Vatican City" => array('cn_iso_2' => 'VA','cn_short_de' => 'Vatikanstadt','size'=> '1000'),
  "Saint Vincent and the Grenadines" => array('cn_iso_2' => 'VC','cn_short_de' => 'St. Vincent und die Grenadinen','size'=> '101390'),
  "Venezuela" => array('cn_iso_2' => 'VE','cn_short_de' => 'Venezuela','size'=> '28644603'),
  "British Virgin Islands" => array('cn_iso_2' => 'VG','cn_short_de' => 'Britische Jungferninseln','size'=> '31196'),

  "Vietnam" => array('cn_iso_2' => 'VN','cn_short_de' => 'Vietnam','size'=> '98721275'),

  "South Africa" => array('cn_iso_2' => 'ZA','cn_short_de' => 'Südafrika','size'=> '56463617'),
  "Zambia" => array('cn_iso_2' => 'ZM','cn_short_de' => 'Sambia','size'=> '17426623'),
  "Zimbabwe" => array('cn_iso_2' => 'ZW','cn_short_de' => 'Simbabwe','size'=> '14546314'),
  "Palestine" => array('cn_iso_2' => 'PS','cn_short_de' => 'Palästina','size'=> '5052000'),

  "Serbia" => array('cn_iso_2' => 'RS','cn_short_de' => 'Serbien','size'=> '7012165'),
  
  "Montenegro" => array('cn_iso_2' => 'ME','cn_short_de' => 'Montenegro','size'=> '609859'),

  "Saint Barthelemy" => array('cn_iso_2' => 'BL','cn_short_de' => 'Saint Barthelemy','size'=> '7122'),
  
  "Republic of Ireland" => array('cn_iso_2' => 'IE','cn_short_de' => 'Irland','size'=> '5176569'),
  "Iran (Islamic Republic of)" => array('cn_iso_2' => 'IR','cn_short_de' => 'Iran','size'=> '84923314'),
  "Republic of Korea" => array('cn_iso_2' => 'KR','cn_short_de' => 'Südkorea','size'=> '56463617'),
  "Hong Kong SAR" => array('cn_iso_2' => 'HK','cn_short_de' => 'Hong Kong','size'=> '7249907'),
  "Taipei and environs" => array('cn_iso_2' => 'TW','cn_short_de' => 'Taiwan','size'=> '23603049'),

  "Russian Federation" => array('cn_iso_2' => 'RU','cn_short_de' => 'Russland','size'=> '141722205'),
  "Macao SAR" => array('cn_iso_2' => 'MO','cn_short_de' => 'Macau','size'=> '614458'),
  "Republic of Moldova" => array('cn_iso_2' => 'MD','cn_short_de' => 'Moldawien','size'=> '3364496'),

 
  "Holy See" => array('cn_iso_2' => 'VA','cn_short_de' => 'Vatikanstadt','size'=> '1000'),
  "Viet Nam" => array('cn_iso_2' => 'VN','cn_short_de' => 'Vietnam','size'=> '98721275'),
  "Cote d'Ivoire" => array('cn_iso_2' => 'CI','cn_short_de' => 'Elfenbeinküste','size'=> '27481086'),
  "China" => array('cn_iso_2' => 'CN','cn_short_de' => 'China','size'=> '1394015977'),
  "Cruise Ship" => array('cn_iso_2' => 'OT','cn_short_de' => 'Diamond Princess','size'=> '3711'),
  "Diamond Princess" => array('cn_iso_2' => 'OT','cn_short_de' => 'Diamond Princess','size'=> '3711', 'lat'=>'35.4437','long'=>'139.6380'),
  "United Kingdom" => array('cn_iso_2' => 'GB','cn_short_de' => 'Vereinigtes Königreich','size'=> '65761117'),
  "Taiwan*" => array('cn_iso_2' => 'TW','cn_short_de' => 'Taiwan','size'=> '23603049'),
  "United States" => array('cn_iso_2' => 'US','cn_short_de' => 'Vereinigte Staaten','size'=> '332639102'),
  "UAE" => array('cn_iso_2' => 'AE','cn_short_de' => 'Vereinigte Arabische Emirate','size'=> '9992083'),
  "Korea, South" => array('cn_iso_2' => 'KR','cn_short_de' => 'Südkorea','size'=> '51835110'),
  
  "Kosovo" => array('cn_iso_2' => 'XK','cn_short_de' => 'Kosovo','size'=> '1932774'),
  "Northern Cyprus" => array('cn_iso_2' => 'NC','cn_short_de' => 'Nordzypern','size'=> '326000'),
  "Eswatini" => array('cn_iso_2' => 'SZ','cn_short_de' => 'Swasiland','size'=> '1104479'),
  "Bahamas" => array('cn_iso_2' => 'BS','cn_short_de' => 'Bahamas','size'=> '337721'),
  "Congo Republic" => array('cn_iso_2' => 'CG','cn_short_de' => 'Republik Kongo','size'=> '101780263'),
  
  "Jersey" => array('cn_iso_2' => 'JE','cn_short_de' => 'Jersey','size'=> '101073'),
  "Curacao" => array('cn_iso_2' => 'CW','cn_short_de' => 'Curacao','size'=> '151345'),
  "Guernsey" => array('cn_iso_2' => 'GG','cn_short_de' => 'Guernsey','size'=> '67052'),
  
  "Sint Maarten" => array('cn_iso_2' => 'SX','cn_short_de' => 'Sint Maarten','size'=> '32556'),
  "Bosnia" => array('cn_iso_2' => 'BA','cn_short_de' => 'Bosnien','size'=> '3835586'),
  
  "South Sudan" => array('cn_iso_2' => 'SS','cn_short_de' => 'Südsudan','size'=> '10561244'),
  
  "Isle of Man" => array('cn_iso_2' => 'IM','cn_short_de' => 'Isle of Man','size'=> '90499'),
  
  
  "American Samoa" => array('cn_iso_2' => 'AS','cn_short_de' => 'Amerikanisch-Samoa','size'=> '49437'),
  "Cocos Islands" => array('cn_iso_2' => 'CC','cn_short_de' => 'Kokosinseln','size'=> '596'),
  "Cook Islands" => array('cn_iso_2' => 'CK','cn_short_de' => 'Cookinseln','size'=> '8574'),
  "Christmas Island" => array('cn_iso_2' => 'CX','cn_short_de' => 'Weihnachtsinsel','size'=> '2205'),
  "Dominica" => array('cn_iso_2' => 'DM','cn_short_de' => 'Dominica','size'=> '74243'),
  "Western Sahara" => array('cn_iso_2' => 'EH','cn_short_de' => 'Westsahara','size'=> '652271'),
  "Micronesia" => array('cn_iso_2' => 'FM','cn_short_de' => 'Mikronesien','size'=> '102436'),
  "Faroes" => array('cn_iso_2' => 'FO','cn_short_de' => 'Färöer','size'=> '51628'),
  "Guinea" => array('cn_iso_2' => 'GN','cn_short_de' => 'Guinea','size'=> '12527440'),
  "Guam" => array('cn_iso_2' => 'GU','cn_short_de' => 'Guam','size'=> '168485'),
  "Kiribati" => array('cn_iso_2' => 'KI','cn_short_de' => 'Kiribati','size'=> '111796'),
  "Comoros" => array('cn_iso_2' => 'KM','cn_short_de' => 'Komoren','size'=> '846281'), 
  "North Korea" => array('cn_iso_2' => 'KP','cn_short_de' => 'Nordkorea','size'=> '25643466'),
  "Lesotho" => array('cn_iso_2' => 'LS','cn_short_de' => 'Lesotho','size'=> '1969334'),
  "Marshall Islands" => array('cn_iso_2' => 'MH','cn_short_de' => 'Marshallinseln','size'=> '77917'),
  "Northern Marianas" => array('cn_iso_2' => 'MP','cn_short_de' => 'Nördliche Marianen','size'=> '51433'),
  "Norfolk Island" => array('cn_iso_2' => 'NF','cn_short_de' => 'Norfolkinsel','size'=> '1748'),
  "Nauru" => array('cn_iso_2' => 'NR','cn_short_de' => 'Nauru','size'=> '11000'),
  "Niue" => array('cn_iso_2' => 'NU','cn_short_de' => 'Niue','size'=> '2000'),  
  "Saint Pierre and Miquelon" => array('cn_iso_2' => 'PM','cn_short_de' => 'Saint-Pierre und Miquelon','size'=> '5347'),
  "Pitcairn Islands" => array('cn_iso_2' => 'PN','cn_short_de' => 'Pitcairinselnn','size'=> '50'),
  "Puerto Rico" => array('cn_iso_2' => 'PR','cn_short_de' => 'Puerto Rico','size'=> '3189068'),  
  "Palau" => array('cn_iso_2' => 'PW','cn_short_de' => 'Palau','size'=> '21685'),
  "Solomon Islands" => array('cn_iso_2' => 'SB','cn_short_de' => 'Salomonen','size'=> '685097'),
  "Saint Helena" => array('cn_iso_2' => 'SH','cn_short_de' => 'St. Helena','size'=> '7862'),
  "São Tomé e Príncipe" => array('cn_iso_2' => 'ST','cn_short_de' => 'São Tomé und Príncipe','size'=> '211122'),
  "Tajikistan" => array('cn_iso_2' => 'TJ','cn_short_de' => 'Tadschikistan','size'=> '8873669'),
  "Tokelau" => array('cn_iso_2' => 'TK','cn_short_de' => 'Tokelau','size'=> '1647'),
  "Turkmenistan" => array('cn_iso_2' => 'TM','cn_short_de' => 'Turkmenistan','size'=> '5528627'),  
  "Tonga" => array('cn_iso_2' => 'TO','cn_short_de' => 'Tonga','size'=> '106095'),
  "Tuvalu" => array('cn_iso_2' => 'TV','cn_short_de' => 'Tuvalu','size'=> '11342'),
  "US Virgin Islands" => array('cn_iso_2' => 'VI','cn_short_de' => 'Amerikanische Jungferninseln','size'=> '106235'),  
  "Vanuatu" => array('cn_iso_2' => 'VU','cn_short_de' => 'Vanuatu','size'=> '298333'),
  "Wallis and Futuna" => array('cn_iso_2' => 'WF','cn_short_de' => 'Wallis und Futuna','size'=> '15854'),
  "Samoa" => array('cn_iso_2' => 'WS','cn_short_de' => 'Samoa','size'=> '203774'),
  "Yemen" => array('cn_iso_2' => 'YE','cn_short_de' => 'Jemen','size'=> '29884405'),
  "Faroe Islands" => array('cn_iso_2' => 'FO','cn_short_de' => 'Färöer','size'=> '51628'), 
  
  "Worldwide" => array('cn_iso_2' => 'WW','cn_short_de' => 'Weltweit'),
/*
  "Heard Island and McDonald Islands" => array('cn_iso_2' => 'HM','cn_short_de' => 'Heard und McDonaldinseln'),  
  "Serbia and Montenegro" => array('cn_iso_2' => 'CS','cn_short_de' => 'Serbien und Montenegro'),
  "United States Minor Outlying Islands" => array('cn_iso_2' => 'UM','cn_short_de' => 'United States Minor Outlying Islands'),
  "Netherlands Antilles" => array('cn_iso_2' => 'AN','cn_short_de' => 'Niederländische Antillen'),
  "Antarctica" => array('cn_iso_2' => 'AQ','cn_short_de' => 'Antarktis'),
  "Bouvet Island" => array('cn_iso_2' => 'BV','cn_short_de' => 'Bouvetinsel'),
  "French Guiana" => array('cn_iso_2' => 'GF','cn_short_de' => 'Französisch-Guayana'),
  "Guadeloupe" => array('cn_iso_2' => 'GP','cn_short_de' => 'Guadeloupe'), 
  "South Georgia and the South Sandwich Islands" => array('cn_iso_2' => 'GS','cn_short_de' => 'Südgeorgien und die Südlichen Sandwichinseln'), 
  "British Indian Ocean Territory" => array('cn_iso_2' => 'IO','cn_short_de' => 'Britisches Territorium im Indischen Ozean'),
  "Martinique" => array('cn_iso_2' => 'MQ','cn_short_de' => 'Martinique'),
  "Reunion" => array('cn_iso_2' => 'RE','cn_short_de' => 'Réunion'),
  "Svalbard" => array('cn_iso_2' => 'SJ','cn_short_de' => 'Svalbard und Jan Mayen'),
  "French Southern Territories" => array('cn_iso_2' => 'TF','cn_short_de' => 'Französische Süd- und Antarktisgebiete'),
  "occupied Palestinian territory" => array('cn_iso_2' => 'PS','cn_short_de' => 'Palästina'), 
  "Swaziland" => array('cn_iso_2' => 'SZ','cn_short_de' => 'Swasiland'),
  "Mayotte" => array('cn_iso_2' => 'YT','cn_short_de' => 'Mayotte'),  
  "Åland Islands" => array('cn_iso_2' => 'AX','cn_short_de' => 'Alandinseln'),
  "Others" => array('cn_iso_2' => 'OT','cn_short_de' => 'Diamond Princess'),  
   "Channel Islands" => array('cn_iso_2' => 'XX','cn_short_de' => 'Kanalinseln'),*/
   
   
  "Burma" => array('cn_iso_2' => 'MM','cn_short_de' => 'Myanmar','size'=> '56590071'),
  "Cabo Verde" => array('cn_iso_2' => 'CV','cn_short_de' => 'Cabo Verde','size'=> '583255'),
  "Congo (Kinshasa)" => array('cn_iso_2' => 'CG','cn_short_de' => 'Republik Kongo','size'=> '101780263'),
  "Congo (Brazzaville)" => array('cn_iso_2' => 'CD','cn_short_de' => 'Kongo','size'=> '5293070'),
  "Czechia" => array('cn_iso_2' => 'CZ','cn_short_de' => 'Tschechien','size'=> '10702498'),
  "Saint Kitts and Nevis" => array('cn_iso_2' => 'KN','cn_short_de' => 'St. Kitts und Nevis','size'=> '55345'),
  "Falkland Islands (Malvinas)" => array('cn_iso_2' => 'FK','cn_short_de' => 'Falklandinseln','size'=> '3198'),
  "Sao Tome and Principe" => array('cn_iso_2' => 'ST','cn_short_de' => 'Sao Tome und Principe','size'=> '211122')
);
?>