<?php
require_once '../database/connection.php';

if(isset($_POST['submit']))
{
   
    if(!empty($_FILES['uploaded_file']))
    {
      $path = "../public/uploads/";
      $fileName= $_FILES['uploaded_file']['name'];
      $path = $path . basename( $_FILES['uploaded_file']['name']);
  
      if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $path)) {

            $string = file_get_contents($path);
            $check=json_validator($string);
            if($check){
                writetoDb($string,$conn,$fileName);
            }
            else{
                echo 'Invalid Input';
            }
       
      } else{
          echo "There was an error uploading the file, please try again!";
      }
    }


}

//JSON Validator function
function json_validator($data=NULL) {
    if (!empty($data)) {
                  @json_decode($data);
                  return (json_last_error() === JSON_ERROR_NONE);
          }
          return false;
  }

  function  writetoDb($string,$conn, $fileName){
      try{

        $string = mysqli_real_escape_string ($conn,$string);
        $date = date('Y-m-d H:i:s');
        $query = mysqli_query($conn, "SELECT * FROM `json_files` WHERE file_names='".$fileName."'");
        if(mysqli_num_rows($query) > 0){
            $sql_statement="UPDATE  json_files SET json_data ='".$string."' WHERE file_names='".$fileName."'";
            if (!(mysqli_query($conn,$sql_statement) )){
                echo("Error description: " . mysqli_error($conn));
            }
            
        
        }else{  
            $sql_statement="INSERT INTO json_files (file_names, json_data, created_at) VALUES ('$fileName',  '$string','$date')";
            mysqli_query($conn,$sql_statement) ;

        }

}catch(Exception $e){
    echo 'Error Occured: ' .$e->getMessage();

}
    
  }
?>