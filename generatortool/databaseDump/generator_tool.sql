-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2020 at 09:33 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `generator_tool`
--

-- --------------------------------------------------------

--
-- Table structure for table `json_files`
--

CREATE TABLE `json_files` (
  `id` int(11) NOT NULL,
  `file_names` varchar(100) NOT NULL,
  `json_data` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `json_files`
--

INSERT INTO `json_files` (`id`, `file_names`, `json_data`, `created_at`, `updated_at`) VALUES
(1, 'translations_de.json', '{\"tabbaritems\":[\"COVID-19\",\"Newsticker\",\"Tagebuch\",\"Sonstiges\"],\"tab1subtitle\":\"Infektionen\",\"tab1updated\":\"Stand:\",\"cardlabels\":[\"Best\\u00e4tigt\",\"Todesf\\u00e4lle\",\"Kuriert\",\"Reproduktionsfaktor\"],\"cardtypes\":[\"Bakteriell\",\"Infektion\",\"Pilz\"],\"miscitems\":[\"Rechtliche\",\"App\",\"FAQ\"],\"legalitems\":[\"Datenschutz\",\"Impressum\"],\"obsolete_title\":\"Versionswarnung\",\"obsolete_message_ios\":\"Diese\",\"obsolete_message_android\":\"Diese\",\"store_name_ios\":\"App\",\"store_name_android\":\"Google\",\"favourite\":\"Favoriten\",\"edit_favourite\":\"Bearbeiten\",\"edit_favourite_header\":\"Favoriten\",\"search\":\"Suche\",\"cancel\":\"Abbrechen\",\"no_results\":\"Keine\",\"no_internet\":\"Derzeit\",\"diary_header\":\"Leiden\",\"tips_header\":\"Tipps\",\"symptom_history\":\"Gesundheitsverlauf\",\"back\":\"Zur\\u00fcck\",\"done\":\"Fertig\",\"as_pdf\":\"ALS\",\"symptom_occurence\":\"Symptomvorkommen\",\"pdf_filename\":\"Symptomvorkommen\",\"pdf_title1\":\"ANALYSE\",\"pdf_title2\":\"SYMPTOMVORKOMMEN\",\"today\":\"Heute\",\"previous_day_title\":\"\\u00c4nderung\",\"population_cases\":\"F\\u00e4lle\",\"trend7days\":\"Trend\",\"travel_warnings\":\"Reisewarnungen\",\"export_title\":\"VERSENDEN\",\"close\":\"Schlie\\u00dfen\",\"noChartDataAvailable\":\"Keine123\"}', '2020-08-24 05:27:32', '2020-08-24 15:14:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `json_files`
--
ALTER TABLE `json_files`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `json_files`
--
ALTER TABLE `json_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
