<?php


require_once 'cronjobConfig.php';



$jsonURL = "https://services1.arcgis.com/0MSEUqKaxRlEPj5g/arcgis/rest/services/Coronavirus_2019_nCoV_Cases/FeatureServer/1/query?where=1%3D1&outFields=*&outSR=4326&f=json";

$date = date("Y-m-d");
$baseurl = "owncsv/";
$year = date("Y");
$month = date("m");

$seconds = date("i");
$hours = date("H");
$char_slash = "/";
$basefilename = "2-COVIDCronjob-19-";
$delimiter = "-";
$extension = ".csv";
$failed = "-FehlendeLaender";

$csvName = $baseurl . $basefilename . $date . $extension;
$csvNameFailed = $baseurl . $basefilename . $date. $delimiter.$failed .$extension;
$csvNameTime = $baseurl . $basefilename . $date .$delimiter.$hours.$seconds .$extension;

$arrayCountries = array();

// 1- Country
// 2- Confirmed Cases
// 3- Deaths
// 4- Recovered
// 5- Latitude
// 6- Longitude
// 7- Country Code
// 8- Size


$jsonData = file_get_contents($jsonURL);
$jsonObj = json_decode($jsonData);


if(($jsonObj->objectIdFieldName == $jsonObj->uniqueIdField->name) && $jsonObj->uniqueIdField->isSystemMaintained){
	
	//SystemMaintained und Vorhanden
	/*



	{
		"attributes":{
			"OBJECTID":1,
			"Province_State":"Alberta",
			"Country_Region":"Canada",
			"Last_Update":1586245530000,
			"Lat":53.9333,
			"Long_":-116.5765,
			"Confirmed":1348,
			"Recovered":0,
			"Deaths":24
		},
		"geometry":{
			"x":-116.57649999999995,
			"y":53.933300000000031
		}
	}

	*/
	  
	$arrayForCSV = array();
	$finalarrayForCSV = array();
	$finalfirstLineForCSV = array();
	$arraynotincountrylist = array ();
	$finalarrayForCSVFailed = array();
	 
	$firstlinearray = array();
	$firstlinearray['province_state'] = 'Provinz';
	$firstlinearray['country_region'] = 'Country';
	$firstlinearray['confirmed'] =  'Confirmed Cases';		
	$firstlinearray['deaths'] =  'Deaths';	
	$firstlinearray['recovered'] =  'Recovered';		
	$firstlinearray['latitude']= 'Latitude';;
	$firstlinearray['longitude']= 'Longitude';
	$firstlinearray['country_code'] = 'Country Code';
	$firstlinearray['size'] = 'Size';
	
	$finalfirstLineForCSV[] = $firstlinearray;
	foreach($jsonObj->features as $feature){
		
		// echo $feature->attributes->Province_State;
		// echo "<br>";
		// echo $feature->attributes->Country_Region;
		// echo "<br>";
		// echo $feature->attributes->Lat;
		// echo "<br>";
		// echo $feature->attributes->Long_;
		// echo "<br>";
		// echo $feature->attributes->Confirmed;
		// echo "<br>";
		// echo $feature->attributes->Recovered;
		// echo "<br>";
		// echo $feature->attributes->Deaths;
		// echo "<br>";
		// echo $feature->geometry->x;
		// echo "<br>";
		// echo $feature->geometry->y;
		// echo "<br>";
		// echo "<br>";
		
		if(!isset($feature->attributes->Country_Region) ||!isset($feature->attributes->Confirmed) || !isset($feature->attributes->Deaths) || !isset($feature->attributes->Recovered) ){
			continue;
		}
		
		$countries = array();
		
		$countries['province_state'] = $feature->attributes->Province_State;
		$countries['country_region'] = $feature->attributes->Country_Region;
		$countries['confirmed'] =  $feature->attributes->Confirmed;		
		$countries['deaths'] =  $feature->attributes->Deaths;	
		$countries['recovered'] =  $feature->attributes->Recovered;		

		$countries['latitude']=$feature->attributes->Lat;
	
		$countries['longitude']=$feature->attributes->Long_;

		
		$indexArray = 0;
		$needTobeAdded = true;

		if(array_key_exists($feature->attributes->Province_State,$static_countries)){
			
			$countries['country_region'] = $feature->attributes->Province_State;
			$countries['country_code']=$static_countries[$feature->attributes->Province_State]['cn_iso_2'];
			$countries['size']=$static_countries[$feature->attributes->Province_State]['size'];

			
			if(isset($countries['country_code']) && isset($countries['size'])){
				foreach($arrayForCSV as $arrayApp){
					

					if($arrayApp['country_region'] == $feature->attributes->Province_State){
						$needTobeAdded = false;
					}
				
				}
				if($needTobeAdded){
					$arrayForCSV[]= $countries;
				}else{
					$countries['country_region'] = $feature->attributes->Country_Region;
					$countries['country_code']=$static_countries[$feature->attributes->Country_Region]['cn_iso_2'];
					$countries['size']=$static_countries[$feature->attributes->Country_Region]['size'];
					foreach($arrayForCSV as $arrayApp){
					

						if($arrayApp['country_region'] == $countries['country_region']){
										
							$arrayApp['confirmed'] +=  $countries['confirmed'];				
							$arrayApp['deaths'] +=  $countries['deaths'];					
							$arrayApp['recovered'] +=  $countries['recovered'];		
								
							if(isset($countries['latitude'])){
								$arrayApp['latitude'] =  $countries['latitude'];		
							}								
							if(isset($countries['longitude'])){
								$arrayApp['longitude'] =  $countries['longitude'];		
							}
							
							$arrayApp['country_code'] =$static_countries[$countries['country_region']]['cn_iso_2'];
							$countries['size']=$static_countries[$countries['country_region']]['size'];
							$arrayForCSV[$indexArray] = $arrayApp;
							$needTobeAdded = false;
						}
						
						$indexArray ++;
					}
				}
			}else{
				$arraynotincountrylist[] = $countries;
			}
			
			
		}else{
			
			$countries['country_code']=$static_countries[$feature->attributes->Country_Region]['cn_iso_2'];
			$countries['size']=$static_countries[$feature->attributes->Country_Region]['size'];
			
			
			if(!isset($countries['latitude'])){
				$countries['latitude']=$static_countries[$feature->attributes->Country_Region]['lat'];
			}
			
			if(!isset($countries['longitude'])){
				$countries['longitude']=$static_countries[$feature->attributes->Country_Region]['long'];
			}
			if(isset($countries['country_code']) && isset($countries['size'])){
				

				foreach($arrayForCSV as $arrayApp){
					

					if($arrayApp['country_region'] == $countries['country_region']){
									
						$arrayApp['confirmed'] +=  $countries['confirmed'];				
						$arrayApp['deaths'] +=  $countries['deaths'];					
						$arrayApp['recovered'] +=  $countries['recovered'];		
							
						if(isset($countries['latitude'])){
							$arrayApp['latitude'] =  $countries['latitude'];		
						}								
						if(isset($countries['longitude'])){
							$arrayApp['longitude'] =  $countries['longitude'];		
						}
						
						$arrayApp['country_code'] =$static_countries[$countries['country_region']]['cn_iso_2'];
						$countries['size']=$static_countries[$countries['country_region']]['size'];
						$arrayForCSV[$indexArray] = $arrayApp;
						$needTobeAdded = false;
					}
					
					$indexArray ++;
				}
				if($needTobeAdded){
					$arrayForCSV[]= $countries;
				}
			}else{
				$arraynotincountrylist[] = $countries;
			}
			
		}
		
		
	}
	
	
	
	usort($arrayForCSV, 
		function($a, $b) {
		 return $a['confirmed'] <= $b['confirmed'];
		}
	);
	
	$finalarrayForCSV = array_merge($finalfirstLineForCSV, $arrayForCSV);

	$fp = fopen($csvName, 'w'); 
  
	// Loop through file pointer and a line 
	foreach ($finalarrayForCSV as $fields) { 
		unset($fields['province_state']);
		fputcsv($fp, $fields,";"); 
	} 
	  
	fclose($fp); 
	
	$fptime = fopen($csvNameTime, 'w'); 
  
	// Loop through file pointer and a line 
	foreach ($finalarrayForCSV as $fields) { 
		unset($fields['province_state']);
		fputcsv($fptime, $fields,";"); 
	} 
	  
	fclose($fptime); 
	
	if(isset($arraynotincountrylist)){
		$finalarrayForCSVFailed = array_merge($finalfirstLineForCSV, $arraynotincountrylist);
		$fp2 = fopen($csvNameFailed, 'w'); 
	  
		// Loop through file pointer and a line 
		foreach ($finalarrayForCSVFailed as $fields) { 
			unset($fields['province_state']);
			fputcsv($fp2, $fields,";"); 
		} 
		  
		fclose($fp2); 
	}
	
}
?>