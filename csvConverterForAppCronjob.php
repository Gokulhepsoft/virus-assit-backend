<?php

require_once 'cronjobConfig.php';


// Datei öffnen, $handle ist der Dateizeiger

// //Problem ist das die Datei erst um 1 Uhr am nächsten Tag hochgeladen wird
// $date = date("m-d-Y");
// $date = "03-11-2020";
// $csvPath = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/";
// $csvPath = "";
// $csvFileName = $date.".csv";
// $csvName = $csvPath . $csvFileName;

// if (($handle = fopen($csvName, "r")) !== FALSE) {
// 	//Take file from today
// }else{
// 	//Take file from yesterday
// 	$date = date("m-d-Y", time() - 60 * 60 * 24);
// 	$csvFileName = $date.".csv";
// 	$csvName = $csvPath . $csvFileName;	
	
// }

// Block for TravellerAssist
$date = date("Y-m-d");
$baseurl = "http://p546259.mittwaldserver.info/owncsv/";
// $baseurl = "owncsv/";
$year = date("Y");
$month = date("m");
$char_slash = "/";
$basefilename = "2-COVIDCronjob-19-";
$extension = ".csv";

$date_before = date( 'Y-m-d', strtotime( $date . ' -1 day' ) );
$month_before = date( 'm', strtotime( $date . ' -1 day' ) );
$year_before = date( 'Y', strtotime( $date . ' -1 day' ) );

$csvName = $baseurl . $basefilename . $date . $extension;
$csvNameDayBefore = $baseurl . $basefilename . $date_before . $extension;

$row = 1;
$rowDayBefore = 1;
$arrayCountriesDaybefore = array();



if (($handleDaybefore = fopen($csvNameDayBefore, "r")) !== FALSE) {
    while (($dataDaybefore = fgetcsv($handleDaybefore, 1000, ";")) !== FALSE) {
	
	    if($rowDayBefore == 1 ){
			if($dataDaybefore[0] != 'Country'){
				die();	
			}
		}
		
		if($rowDayBefore != 1) {
			$arrayCountryDaybefore = array();
			$arrayCountryDaybefore['countryname']= $dataDaybefore[0];
			$arrayCountryDaybefore['confirmed']= $dataDaybefore[1];
			$arrayCountryDaybefore['deaths']= $dataDaybefore[2];
			$arrayCountryDaybefore['recovered']= $dataDaybefore[3];
			$arrayCountryDaybefore['latitude']= $dataDaybefore[4];
			$arrayCountryDaybefore['longitude']= $dataDaybefore[5];
			$arrayCountryDaybefore['countrycode']= $dataDaybefore[6];

			
			
			$arrayCountriesDaybefore[]= $arrayCountryDaybefore;
		}
		$rowDayBefore++;
	}
}

//echo "<pre>".var_dump($arrayCountriesDaybefore)."</pre><br>";
//print_r(array_values($arrayCountriesDaybefore));


$arrayCountries = array();

if (($handle = fopen($csvName, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

		// if($row != 1){
		// 	$arrayCountry = array();
		// 	$arrayCountry['countryname']= $data[1];
		// 	$arrayCountry['provinz']= $data[0];
		// 	$arrayCountry['last_update']= $data[2];
		// 	$arrayCountry['confirmed']= $data[3];
		// 	$arrayCountry['deaths']= $data[4];
		// 	$arrayCountry['recovered']= $data[5];
		// 	$arrayCountry['latitude']= $data[6];
		// 	$arrayCountry['longitude']= $data[7];
		// 	$arrayCountries[]= $arrayCountry;

    // }

    if($row == 1 ){
      if($data[0] != 'Country'){
        die();	
      }
    }

    if($row != 1) {
      $arrayCountry = array();
      	$arrayCountry['countryname']= $data[0];
      	$arrayCountry['confirmed']= $data[1];
        $arrayCountry['deaths']= $data[2];
        $arrayCountry['recovered']= $data[3];
      	$arrayCountry['latitude']= $data[4];
      	$arrayCountry['longitude']= $data[5];
      	$arrayCountry['countrycode']= $data[6];
		$deathspercent = 0;
		$confirmedpercent = 0;
		
		$arrayCountry['countrysize']= $data[7];
					
		if($data[2] !== 0){
			$deathspercent = (($data[2] / $data[7]) *100);
		}
		if($data[1] !== 0){
			$confirmedpercent = (($data[1] / $data[7]) * 100);
		}
		if($deathspercent > 0 && $deathspercent <= 0.01 ){
			$deathspercent = "<0,01";
		}
		
		if($deathspercent != "<0,01"){
				$deathspercent	=	number_format($deathspercent,2,",",".");
		}
		if($confirmedpercent > 0 && $confirmedpercent <= 0.01 ){
			$confirmedpercent = "<0,01";
		}
		
		if($confirmedpercent != "<0,01"){
				$confirmedpercent	=	number_format($confirmedpercent,2,",",".");
		}
		
		$deathspercent .= "%";
		$confirmedpercent .= "%";
					
		$arrayCountry['countryConfirmedPercent']= $confirmedpercent;
		$arrayCountry['countryDeathsPercent']= $deathspercent;
		
		
		
		
		
      	$arrayCountries[]= $arrayCountry;
    }
		$row++;
    }
    fclose($handle);
	
	//CSV so bearbeiten das Sie für die App genutzt werden kann
	$arrayForApp = array();
	foreach($arrayCountries as $countries){
		
		// unset($countries['latitude']);
		// unset($countries['longitude']);
		$needTobeAdded = true;
		$indexArray = 0;
		$countryRecoveredDaybefore=0;
		$countryDeathsDaybefore=0;
		$countryRecoveredDaybefore=0;
		
		foreach($arrayCountriesDaybefore as $countriesDaybefore){
			if($countriesDaybefore['countryname'] == $countries['countryname']){
				$countryConfirmedDaybefore =  $countriesDaybefore['confirmed'];	
				$countryDeathsDaybefore =  $countriesDaybefore['deaths'];
				$countryRecoveredDaybefore =  $countriesDaybefore['recovered'];
			}
		}
		$countries['confirmedDayBefore'] = $countryConfirmedDaybefore;
		$countries['deathsDayBefore'] = $countryDeathsDaybefore;
		$countries['recoveredDayBefore'] = $countryRecoveredDaybefore;
		
		
		$deathsTrendpercent = 0;
		$confirmedTrendpercent = 0;
		if($countryDeathsDaybefore != 0 ){	
			$deathsTrendpercent = (($countries['deaths'] / $countryDeathsDaybefore) *100) -100;
		}
		if($countryConfirmedDaybefore != 0){
		$confirmedTrendpercent = (($countries['confirmed'] / $countryConfirmedDaybefore) * 100) -100;
		
			
			
		}
		
		$confirmedTrendpercentString = '';
		$deathsTrendpercentString = '';
		
		$countries['countryConfirmedTrendPercent']= $confirmedTrendpercent;
		$countries['countryDeathsTrendPercent']= $deathsTrendpercent;

		if($deathsTrendpercent > 0 && $deathsTrendpercent <= 0.01 ){
			$deathsTrendpercentString = "<0,01";
		}
		
		if($deathsTrendpercent != "<0,01"){
				$deathsTrendpercentString	=	number_format($deathsTrendpercent,2,",",".");
		}
		if($confirmedTrendpercent > 0 && $confirmedTrendpercent <= 0.01 ){
			$confirmedTrendpercentString = "<0,01";
		}
		
		if($confirmedTrendpercent != "<0,01"){
				$confirmedTrendpercentString	=	number_format($confirmedTrendpercent,2,",",".");
		}
		
		
		
		if(($deathsTrendpercent == "<0,01" || $deathsTrendpercent > 0) && $deathsTrendpercent != 0 ){
			$deathsTrendpercentString = "+".$deathsTrendpercentString;
		}
		
		if($deathsTrendpercent === 0){
			$deathsTrendpercentString = "+/-".$deathsTrendpercent;
		}
		
		
		
		if(($confirmedTrendpercent == "<0,01" || $confirmedTrendpercent > 0) && $confirmedTrendpercent !== 0){
			$confirmedTrendpercentString = "+".$confirmedTrendpercentString;
		}
		
		if($confirmedTrendpercent === 0){
			$confirmedTrendpercentString = "+/-".$confirmedTrendpercent;
		}
		$deathsTrendpercentString .= "%";
		$confirmedTrendpercentString .= "%";
					

		$countries['countryConfirmedTrendPercentString']= $confirmedTrendpercentString;
		$countries['countryDeathsTrendPercentString']= $deathsTrendpercentString;
	
		
		$countryDifferentdeathsDaybefore =0;
		$countryDifferentconfirmedDaybefore = 0;
		
		$countryDifferentconfirmedDaybefore = number_format($countries['confirmed'] - $countries['confirmedDayBefore'] ,0,",",".");
		
		$countryDifferentdeathsDaybefore = number_format($countries['deaths'] - $countries['deathsDayBefore'] ,0,",",".");
		
		if($countryDifferentdeathsDaybefore > 0){
			$countryDifferentdeathsDaybefore = "+".$countryDifferentdeathsDaybefore;
		}
		
		if($countryDifferentconfirmedDaybefore > 0){
			$countryDifferentconfirmedDaybefore = "+".$countryDifferentconfirmedDaybefore;
		}
		$countries['deathsDifferentfromDaybefore'] = $countryDifferentdeathsDaybefore;
		$countries['confirmedDifferentfromDaybefore'] = $countryDifferentconfirmedDaybefore ;
	
	
		
		foreach($arrayForApp as $arrayApp){
			

			if($arrayApp['countryname'] == $countries['countryname']){
							
				$arrayApp['confirmed'] +=  $countries['confirmed'];				
				$arrayApp['deaths'] +=  $countries['deaths'];					
				$arrayApp['recovered'] +=  $countries['recovered'];		
				
				$arrayApp['confirmedDayBefore'] =  $countries['confirmedDayBefore'];
				$arrayApp['deathsDayBefore'] =  $countries['deathsDayBefore'];
				$arrayApp['recoveredDayBefore'] =  $countries['recoveredDayBefore'];
				
				
				

				
				
				$arrayForApp[$indexArray] = $arrayApp;
				$needTobeAdded = false;
			}
			
			$indexArray ++;
		}
		if($needTobeAdded){
			$arrayForApp[]= $countries;
		}

	}

	
	$globalAppArray = array();
	$globalAppArray['countryname'] = "Worldwide";
	$globalAppArray['countrycode'] =  $static_countries[$globalAppArray['countryname']]['cn_iso_2'];	
	foreach($arrayForApp as $arrayApp){
		$globalAppArray['confirmed'] += $arrayApp['confirmed'];
		$globalAppArray['deaths'] += $arrayApp['deaths'];
		$globalAppArray['recovered'] += $arrayApp['recovered'];
		
		$globalAppArray['confirmedDayBefore'] += $arrayApp['confirmedDayBefore'];
		$globalAppArray['deathsDayBefore'] += $arrayApp['deathsDayBefore'];
		$globalAppArray['recoveredDayBefore'] += $arrayApp['recoveredDayBefore'];
		
		$globalAppArray['countrysize'] += $arrayApp['countrysize'];
		
	}
	$globalAppArray['confirmed'] = $letterQuats.$globalAppArray['confirmed'].$letterQuats;
	$globalAppArray['deaths'] = $letterQuats.$globalAppArray['deaths'].$letterQuats;
	$globalAppArray['recovered'] = $letterQuats.$globalAppArray['recovered'].$letterQuats;
	$globalAppArray['confirmedDayBefore'] = $letterQuats.$globalAppArray['confirmedDayBefore'].$letterQuats;
	$globalAppArray['deathsDayBefore'] = $letterQuats.$globalAppArray['deathsDayBefore'].$letterQuats;
	$globalAppArray['recoveredDayBefore'] = $letterQuats.$globalAppArray['recoveredDayBefore'].$letterQuats;
	$globalAppArray['countrysize'] = $letterQuats.$globalAppArray['countrysize'].$letterQuats;
	
	
		$deathspercent = 0;
		$confirmedpercent = 0;
	
		if($globalAppArray['deaths']!== 0){
			$deathspercent = (($globalAppArray['deaths'] / $globalAppArray['countrysize'] ) *100);
		}
		if($globalAppArray['confirmed'] !== 0){
			$confirmedpercent = (($globalAppArray['confirmed'] / $globalAppArray['countrysize'] ) * 100);
		}
		if($deathspercent > 0 && $deathspercent <= 0.01 ){
			$deathspercent = "<0,01";
		}
		
		if($deathspercent != "<0,01"){
				$deathspercent	=	number_format($deathspercent,2,",",".");
		}
		if($confirmedpercent > 0 && $confirmedpercent <= 0.01 ){
			$confirmedpercent = "<0,01";
		}
		
		if($confirmedpercent != "<0,01"){
				$confirmedpercent	=	number_format($confirmedpercent,2,",",".");
		}
		
		$deathspercent .= "%";
		$confirmedpercent .= "%";
					
		$globalAppArray['countryConfirmedPercent']= $confirmedpercent;
		$globalAppArray['countryDeathsPercent']= $deathspercent;
		
		$globalDifferentdeathsDaybefore =0;
		$globalDifferentconfirmedDaybefore = 0;
		
		$globalDifferentconfirmedDaybefore = number_format($globalAppArray['confirmed'] - $globalAppArray['confirmedDayBefore'] ,0,",",".");
		
		$globalDifferentdeathsDaybefore = number_format($globalAppArray['deaths'] - $globalAppArray['deathsDayBefore'] ,0,",",".");
		
		if($globalDifferentdeathsDaybefore > 0){
			$globalDifferentdeathsDaybefore = "+".$globalDifferentdeathsDaybefore;
		}
		
		if($globalDifferentconfirmedDaybefore > 0){
			$globalDifferentconfirmedDaybefore = "+".$globalDifferentconfirmedDaybefore;
		}
		$globalAppArray['deathsDifferentfromDaybefore'] = $globalDifferentdeathsDaybefore;
		$globalAppArray['confirmedDifferentfromDaybefore'] = $globalDifferentconfirmedDaybefore ;
	
	
		$deathsTrendpercent = 0;
		$confirmedTrendpercent = 0;
		if($globalAppArray['deathsDayBefore'] != 0 ){	
			$deathsTrendpercent = (($globalAppArray['deaths'] / $globalAppArray['deathsDayBefore']) *100) -100;
		}
		if($globalAppArray['confirmedDayBefore'] != 0){
		$confirmedTrendpercent = (($globalAppArray['confirmed'] / $globalAppArray['confirmedDayBefore']) * 100) -100;
		
			
			
		}
		
		$confirmedTrendpercentString = '';
		$deathsTrendpercentString = '';
		
		$globalAppArray['countryConfirmedTrendPercent']= $confirmedTrendpercent;
		$globalAppArray['countryDeathsTrendPercent']= $deathsTrendpercent;

		if($deathsTrendpercent > 0 && $deathsTrendpercent <= 0.01 ){
			$deathsTrendpercentString = "<0,01";
		}
		
		if($deathsTrendpercent != "<0,01"){
				$deathsTrendpercentString	=	number_format($deathsTrendpercent,2,",",".");
		}
		if($confirmedTrendpercent > 0 && $confirmedTrendpercent <= 0.01 ){
			$confirmedTrendpercent = "<0,01";
		}
		
		if($confirmedTrendpercent != "<0,01"){
				$confirmedTrendpercentString	=	number_format($confirmedTrendpercent,2,",",".");
		}
		
		
		
		if(($deathsTrendpercent == "<0,01" || $deathsTrendpercent > 0) && $deathsTrendpercent != 0 ){
			$deathsTrendpercentString = "+".$deathsTrendpercentString;
		}
		
		if($deathsTrendpercent === 0){
			$deathsTrendpercentString = "+/-".$deathsTrendpercent;
		}
		
		if(($confirmedTrendpercent == "<0,01" || $confirmedTrendpercent > 0)){
			$confirmedTrendpercentString = "+".$confirmedTrendpercentString;
		}
		
		if($confirmedTrendpercent === 0){
			$confirmedTrendpercentString = "+/-".$confirmedTrendpercentString;
		}
		$deathsTrendpercentString .= "%";
		$confirmedTrendpercentString .= "%";
					

		$globalAppArray['countryConfirmedTrendPercentString']= $confirmedTrendpercentString;
		$globalAppArray['countryDeathsTrendPercentString']= $deathsTrendpercentString;
		
		
		
	
	$indexArrayUebersetzung = 0;
	foreach($arrayForApp as $arrayApp){
		
		
		if(array_key_exists($arrayApp['countryname'],$static_countries)){
			$arrayApp['name'] =  $static_countries[$arrayApp['countryname']]['cn_short_de'];				
			
			$arrayForApp[$indexArrayUebersetzung] = $arrayApp;
		}
		$indexArrayUebersetzung++;
	}
	
	$indexArrayUebersetzungGlobal = 0;			
	if(array_key_exists($globalAppArray['countryname'],$static_countries)){
	
		$globalAppArray['name'] =  $static_countries[$globalAppArray['countryname']]['cn_short_de'];				
		//$globalAppArray[$indexArrayUebersetzungGlobal] = $globalAppArray;
	}

	$arrayForAppFinal = array();
	$arrayForAppFinal[]= $globalAppArray;
	
	
	
	usort($arrayForApp, 
		function($a, $b) {
		 return $a['confirmed'] <= $b['confirmed'];
		}
	);
	
	//Array in CSV verwandeln oder jegliche gewünschte Format
	foreach($arrayForApp as $arrayAppFinal){
		
		if( $arrayAppFinal['countryname'] != "Worldwide" && isset($arrayAppFinal['countryname'])){
			$arrayForAppFinal[] = $arrayAppFinal;
		}
	}
		
	$confirmedString = '';
	$confirmed2String = '';
	$nameString = '';
	$nameEnglishString = '';
	$recoveredString = '';
	$deathsstring = '';
	$risikoString = '';
	$radiusString = '';
	$artString = '';
	$artvirusString = '';
	$countrycodeSortedString = '';
	$countrycodeTravelSortedString = '';
	$nameTravelString = '';
	$letterQuats = '';	
	$letterComma = ',';
	
		
	$confirmedDayBeforeString = '';
	$deathsDayBeforeString = '';
	$recovredDayBeforeString = '';
	
	$arrayArt = array("Virus");

	
	// $arrayArt = array("Epedimie","Naturkatastrophen");
	foreach($arrayForAppFinal as $arrayAppFinal){
	
		if($arrayAppFinal['name'] == "Weltweit"){
			$confirmedString .= $letterQuats.$arrayAppFinal['confirmed'].$letterQuats.$letterComma;
		}
		else{
			$confirmedString .= $letterQuats.number_format($arrayAppFinal['confirmed'],0,",",".").$letterQuats.$letterComma;
		}
		
		
		$confirmed2String .= $letterQuats.number_format($arrayAppFinal['confirmed'],0,",",".").$letterQuats.$letterComma;
		
		if(isset($arrayAppFinal['name'])){
			$nameString .= $letterQuats.utf8_decode($arrayAppFinal['name']).$letterQuats.$letterComma;
		}else{
			$nameString .= $letterQuats.utf8_decode($arrayAppFinal['countryname']).$letterQuats.$letterComma;
		}
		$nameEnglishString .= $letterQuats.utf8_decode($arrayAppFinal['countryname']).$letterQuats.$letterComma;
		$recoveredString .= $letterQuats.number_format($arrayAppFinal['recovered'],0,",",".").$letterQuats.$letterComma;
    $deathsstring .= $letterQuats.number_format($arrayAppFinal['deaths'],0,",",".").$letterQuats.$letterComma;
    if(isset($arrayAppFinal['countrycode'])){
      $countrycodeSortedString .= $letterQuats.$arrayAppFinal['countrycode'].$letterQuats.$letterComma;
    } else {
      $countrycodeSortedString .= $letterQuats."XX".$letterQuats.$letterComma;
    }
    $artvirusString .= $letterQuats.$arrayArt[0].$letterQuats.$letterComma;
			
	$confirmedDayBeforeString .= $letterQuats.number_format(($arrayAppFinal['confirmed'] - $arrayAppFinal['confirmedDayBefore']),0,",",".").$letterQuats.$letterComma;
	$deathsDayBeforeString .= $letterQuats.number_format(($arrayAppFinal['deaths'] - $arrayAppFinal['deathsDayBefore']),0,",",".").$letterQuats.$letterComma;
	$recovredDayBeforeString .= $letterQuats.number_format(($arrayAppFinal['recovered'] - $arrayAppFinal['recoveredDayBefore']),0,",",".").$letterQuats.$letterComma;
	
		// if($arrayAppFinal['name'] != "Weltweit"){
		// 	$risikoString .= $letterQuats.number_format((($arrayAppFinal['confirmed']/$globalAppArray['confirmed'])*100),2,".",",").$letterQuats.$letterComma;
		// 	$warnString .= $letterQuats."Nein".$letterQuats.$letterComma;
		// 	$zahl = rand(0,(sizeof($arrayArt)-1));
		// 	$artString .= $letterQuats.$arrayArt[$zahl].$letterQuats.$letterComma;
		// 	$countrycodeTravelSortedString .= $letterQuats.$arrayAppFinal['countrycode'].$letterQuats.$letterComma;
		// 	$nameTravelString .= $letterQuats.utf8_decode($arrayAppFinal['name']).$letterQuats.$letterComma;
		// }
  }
  
	$artvirusString = rtrim($artvirusString, $letterComma);
	$confirmedString = rtrim($confirmedString, $letterComma);
	$deathsstring = rtrim($deathsstring, $letterComma);
	$recoveredString = rtrim($recoveredString, $letterComma);
	$nameString = rtrim($nameString, $letterComma);
	$nameEnglishString = rtrim($nameEnglishString, $letterComma);
	$countrycodeSortedString = rtrim($countrycodeSortedString, $letterComma);
	$lastupdatedString = $letterQuats.date("d.m.Y").$letterQuats;
	
	$confirmedDayBeforeString = rtrim($confirmedDayBeforeString, $letterComma);
	$deathsDayBeforeString = rtrim($deathsDayBeforeString, $letterComma);
	$recovredDayBeforeString = rtrim($recovredDayBeforeString, $letterComma);

	// $risikoString = rtrim($risikoString, $letterComma);
	// $warnString = rtrim($warnString, $letterComma);
	// $artString = rtrim($artString, $letterComma);
	// $nameTravelString = rtrim($nameTravelString, $letterComma);
	// $countrycodeTravelSortedString = rtrim($countrycodeTravelSortedString, $letterComma);

	file_put_contents("cronjob_v_homescreen.json",json_encode($arrayForAppFinal));
  //Export for TravellerAssist
	file_put_contents("cronjob_t_name.txt", $nameString);
	file_put_contents("cronjob_t_nameenglish.txt", $nameEnglishString);
	file_put_contents("cronjob_t_confirmed.txt", $confirmedString);
	file_put_contents("cronjob_t_deaths.txt", $deathsstring);
  file_put_contents("cronjob_t_artvirus.txt", $artvirusString);
  file_put_contents("cronjob_t_lastupdated.txt", $lastupdatedString);
  file_put_contents("cronjob_t_countrycodesorted.txt", $countrycodeSortedString);
  
  	file_put_contents("cronjob_t_confirmedDaybefore.txt", $confirmedDayBeforeString);
	file_put_contents("cronjob_t_deathsDaybefore.txt", $deathsDayBeforeString);
	file_put_contents("cronjob_t_recoveredDaybefore.txt", $recovredDayBeforeString);
  
  //Export for VirusAssist
	file_put_contents("cronjob_v_name.txt", $nameString);
	file_put_contents("cronjob_v_confirmed.txt", $confirmedString);
	
	file_put_contents("cronjob_v2_confirmed.txt", $confirmed2String);
	file_put_contents("cronjob_v_deaths.txt", $deathsstring);
	file_put_contents("cronjob_v_recovered.txt", $recoveredString);
  file_put_contents("cronjob_v_lastupdated.txt", $lastupdatedString);
  file_put_contents("cronjob_v_countrycodesorted.txt", $countrycodeSortedString);
  
	file_put_contents("cronjob_v_confirmedDaybefore.txt", $confirmedDayBeforeString);
	file_put_contents("cronjob_v_deathsDaybefore.txt", $deathsDayBeforeString);
	file_put_contents("cronjob_v_recoveredDaybefore.txt", $recovredDayBeforeString);
  
  //Export for VirusAssist static 1.0.0
	file_put_contents("name.txt", $nameString);
	file_put_contents("confirmed.txt", $confirmedString);
	file_put_contents("deaths.txt", $deathsstring);
	file_put_contents("recovered.txt", $recoveredString);
  file_put_contents("lastupdated.txt", $lastupdatedString);

	file_put_contents("confirmedDaybefore.txt", $confirmedDayBeforeString);
	file_put_contents("deathsDaybefore.txt", $deathsDayBeforeString);
	file_put_contents("confirmedDayBeforeStrings.txt", $confirmedDayBeforeStrings);
	
	// file_put_contents("risiko.txt", $risikoString);	
	// file_put_contents("warn.txt", $warnString);	
	// file_put_contents("art.txt", $artString);	
	// file_put_contents("nameTravel.txt", $nameTravelString);
	// file_put_contents("countrycodeTravelsorted.txt", $countrycodeTravelSortedString);
		
	//Circle calculation	
	$namenString = '';
	$countrycodeString = '';
	$latitudeString = '';
	$longitudeString = '';
	foreach($arrayForApp as $countries){
		
		$countrycodeString .= $letterQuats.$static_countries[$countries['countryname']]['cn_iso_2'].$letterQuats.$letterComma;
		$latitudeString .= $letterQuats.$countries['latitude'].$letterQuats.$letterComma;
		$longitudeString .= $letterQuats.$countries['longitude'].$letterQuats.$letterComma;
		
		$radius = 0;
		$radius = ((sqrt($countries['confirmed'] / pi()))*5000)/2;
		$radiusString .= $letterQuats.$radius.$letterQuats.$letterComma;
	}
	
	$latitudeString = rtrim($latitudeString, $letterComma);
	$longitudeString = rtrim($longitudeString, $letterComma);
	$radiusString = rtrim($radiusString, $letterComma);
	$countrycodeString = rtrim($countrycodeString, $letterComma);
    
  
  //Export for TravellerAssist
	file_put_contents("cronjob_t_radius.txt", $radiusString);
	file_put_contents("cronjob_t_latitude.txt", $latitudeString);
	file_put_contents("cronjob_t_longitude.txt", $longitudeString);
  file_put_contents("cronjob_t_countrycode.txt", $countrycodeString);
  
  //Export for VirusAssist
	file_put_contents("cronjob_v_radius.txt", $radiusString);
	file_put_contents("cronjob_v_latitude.txt", $latitudeString);
	file_put_contents("cronjob_v_longitude.txt", $longitudeString);
	file_put_contents("cronjob_v_countrycode.txt", $countrycodeString);
	
	//echo '<pre>';
	//echo var_dump($arrayForAppFinal) ;
	//echo '</pre>';
	
	

	$arrayCountries14Days = array();
	/*
	$arrayForAppFinalIndex = 0;
	
	foreach($arrayForAppFinal as $countries){
		$deathscountry = number_format($countries['deaths'],0,",",".");
		$confirmedcountry = number_format($countries['confirmed'],0,",",".");
		$recoveredcountry = number_format($countries['recovered'],0,",",".");
		$arrayForAppFinal[$arrayForAppFinalIndex]['deaths'] = $deathscountry;
		$arrayForAppFinal[$arrayForAppFinalIndex]['confirmed'] = $confirmedcountry;
		$arrayForAppFinal[$arrayForAppFinalIndex]['recovered'] = $confirmedcountry;
		
		$arrayForAppFinalIndex++;
	}*/
	
	$arrayCountries14Days [] = $arrayForAppFinal;
	
	for($intI = 1; $intI <=14;$intI++){
		$date_before = date( 'Y-m-d', strtotime( $date . ' -'.$intI.' day' ) );
		$year_before = date( 'Y', strtotime( $date . ' -'.$intI.' day' ) );
		$month_before = date( 'm', strtotime( $date . ' -'.$intI.' day' ) );
	
		$csvName14DaysBefore = $baseurl .$basefilename . $date_before . $extension;
		
		$row14Daysbefore = 1;
		
		$arrayCountries = array();
		if (($handle14DaysBefore = fopen($csvName14DaysBefore, "r")) !== FALSE) {
		
			while (($data14DaysBefore = fgetcsv($handle14DaysBefore, 1000, ";")) !== FALSE) {
				
				if($row14Daysbefore == 1 ){

					$isRightCSVCorrection = true;
					if($data14DaysBefore[0] != 'Country'){
						$isRightCSVCorrection = false;
					}
				  
					if($data14DaysBefore[1] != 'Confirmed Cases'){
						$isRightCSVCorrection = false;	
					}
					  
					if($data14DaysBefore[2] != 'Deaths'){
						$isRightCSVCorrection = false;	
					}
					  
					if($data14DaysBefore[3] != 'Recovered'){
						$isRightCSVCorrection = false;
					}
					if($data14DaysBefore[4] != 'Latitude'){
						$isRightCSVCorrection = false;	
					}
					if($data14DaysBefore[5] != 'Longitude'){
						$isRightCSVCorrection = false;	
					}
					  
					if($data14DaysBefore[6] != 'Country Code'){
						$isRightCSVCorrection = false;
					}
				  
				//echo $csvName14DaysBefore."<br>";
				}

				
				if($isRightCSVCorrection){
					if($row14Daysbefore != 1) {
						$arrayCountry14DaysBefore = array();
						
						$arrayCountry14DaysBefore['countryname']= $data14DaysBefore[0];
						$arrayCountry14DaysBefore['name']= $static_countries[$data14DaysBefore[0]]['cn_short_de'];
						$arrayCountry14DaysBefore['confirmed']= $data14DaysBefore[1];
						$arrayCountry14DaysBefore['deaths']= $data14DaysBefore[2];
						$arrayCountry14DaysBefore['recovered']= $data14DaysBefore[3];
						$arrayCountry14DaysBefore['countrycode']= $data14DaysBefore[6];
						
						$arrayCountries[]= $arrayCountry14DaysBefore;
					}
				}
				$row14Daysbefore++;
					
					
			}
			fclose($handle14DaysBefore);
			
			
			
			$globalAppArray = array();
			$globalAppArray['countryname'] = "Worldwide";
			$globalAppArray['name'] = "Weltweit";
			foreach($arrayCountries as $arrayApp){
				$globalAppArray['confirmed'] += $arrayApp['confirmed'];
				$globalAppArray['deaths'] += $arrayApp['deaths'];
				$globalAppArray['recovered'] += $arrayApp['recovered'];

			}
			$globalAppArray['confirmed'] = $letterQuats.$globalAppArray['confirmed'].$letterQuats;
			$globalAppArray['deaths'] = $letterQuats.$globalAppArray['deaths'].$letterQuats;
			$globalAppArray['recovered'] = $letterQuats.$globalAppArray['recovered'].$letterQuats;
			$globalAppArray['countrycode'] =  $static_countries[$globalAppArray['countryname']]['cn_iso_2'];	
			
			

			$arrayForAppFinal = array();
			$arrayForAppFinal[]= $globalAppArray;
			
			
			usort($arrayCountries, 
				function($a, $b) {
				 return $a['confirmed'] <= $b['confirmed'];
				}
			);
			
			//Array in CSV verwandeln oder jegliche gewünschte Format
			foreach($arrayCountries as $arrayAppFinal){
				
				if($arrayAppFinal['countryname'] != "Worldwide" && isset($arrayAppFinal['countryname'])){
					$arrayForAppFinal[] = $arrayAppFinal;
				}
			}
			
			
			
			
			if($isRightCSVCorrection){
				$arrayCountries14Days[] =  $arrayForAppFinal;
			}
				

			
		}
		
		
		
	}
	
	
	
	
	file_put_contents("fourteenDaysTrend/cronjob_v_14DaysTrend.json",json_encode($arrayCountries14Days));
	
	
	
	
	
		
}



?>