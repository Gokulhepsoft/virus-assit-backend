<?php

require_once 'cronjobConfig.php';
require_once 'dbconnection.php';
require 'config.php';

$url =$ConFig['wpUrl'];
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json'));
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$data = curl_exec($curl);
if($data===false){
    
}
else{
    $timenDate = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $data), true );
    $timemodified=$timenDate['modifiedTime'];
    $sql = "SELECT file_updated_time FROM countrydetails";
    $result = mysqli_query($conn, $sql);
    
   
if (mysqli_num_rows($result) > 0) {

    $result=mysqli_fetch_row($result);
    $latupdatedDbtime=$result[0];

    if($timemodified > $latupdatedDbtime ){

       
$date = date("Y-m-d");
$baseurl = "http://p546259.mittwaldserver.info/owncsv/";
$year = date("Y");
$month = date("m");
$char_slash = "/";
$basefilename = "2-COVIDCronjob-19-";
$extension = ".csv";
$date_before = date( 'Y-m-d', strtotime( $date . ' -1 day' ) );
$month_before = date( 'm', strtotime( $date . ' -1 day' ) );
$year_before = date( 'Y', strtotime( $date . ' -1 day' ) );

$csvName = $baseurl . $basefilename.$date.$extension;
$csvNameDayBefore = $baseurl . $basefilename . $date_before . $extension;

$row = 1;
$rowDayBefore = 1;
$arrayCountriesDaybefore = array();

//for getting data before today
if (($handleDaybefore = fopen($csvNameDayBefore, "r")) !== FALSE) {
    while (($dataDaybefore = fgetcsv($handleDaybefore, 1000, ";")) !== FALSE) {
	
	    if($rowDayBefore == 1 ){
			if($dataDaybefore[0] != 'Country'){
				die();	
			}
		}
		if($rowDayBefore != 1) {
			$arrayCountryDaybefore = array();
			$arrayCountryDaybefore['countryname']= $dataDaybefore[0];
			$arrayCountryDaybefore['confirmed']= $dataDaybefore[1];
			$arrayCountryDaybefore['deaths']= $dataDaybefore[2];
			$arrayCountryDaybefore['recovered']= $dataDaybefore[3];
			$arrayCountryDaybefore['latitude']= $dataDaybefore[4];
			$arrayCountryDaybefore['longitude']= $dataDaybefore[5];
			$arrayCountryDaybefore['countrycode']= $dataDaybefore[6];
			$arrayCountriesDaybefore[]= $arrayCountryDaybefore;
		}
		$rowDayBefore++;
	}
}

//for getting today's data
$beforeConfirmed=0;
$beforeDeaths=0;
$beforeRecovered =0;
$confirmed =0;
$deaths    = 0;
$recovered =0;


$arrayCountries = array();
if (($handle = fopen($csvName, "r")) !== FALSE) {
	
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
    if($row == 1 ){
      if($data[0] != 'Country'){
        die();	
      }
    }

    if($row != 1) {
      	$arrayCountry = array();
      	$arrayCountry['countryname']= $data[0];
      	$arrayCountry['confirmed']= $data[1];
        $arrayCountry['deaths']= $data[2];
        $arrayCountry['recovered']= $data[3];
      	// $arrayCountry['latitude']= $data[4];
      	// $arrayCountry['longitude']= $data[5];
		$arrayCountry['countrycode']= $data[6];
		
		foreach($arrayCountriesDaybefore as $before){
			if(($arrayCountry['countryname']==$before['countryname']) && ( $arrayCountry['countrycode']==$before['countrycode']) ){

				//$beforeArray = array('confirmedBefore'=>number_format($before['confirmed'],0,",","."),'deathsBefore'=>number_format($before['deaths'],0,",","."),'recoveredBefore'=>number_format($before['recovered'],0,",","."));
				$confirmedDayBeforeString = number_format(($arrayCountry['confirmed'] - $before['confirmed']),0,",",".");
				$deathsDayBeforeString    = number_format(($arrayCountry['deaths'] - $before['deaths']),0,",",".");
				$recovredDayBeforeString  = number_format(($arrayCountry['recovered'] - $before['recovered']),0,",",".");
				$beforeArray = array('confirmedBefore'=>$confirmedDayBeforeString,'deathsBefore'=>$deathsDayBeforeString,'recoveredBefore'=>$recovredDayBeforeString);
				$beforeConfirmed +=$before['confirmed'];
				$beforeDeaths +=$before['deaths'];
				$beforeRecovered +=$before['recovered'];

			}
		}
		$rFactornTrends=getTrendsnRfactor($static_countries[$arrayCountry['countryname']]['cn_iso_2']);

		
		
		if($rFactornTrends['rfactor']!=-1234 ){
			$rFactor = $rFactornTrends['rfactor'];
			$beforeArray['rfactor']=$rFactornTrends['trend'];
			$dataArray =array('countryName'=>$arrayCountry['countryname'],'countryCode'=>$static_countries[$arrayCountry['countryname']]['cn_iso_2'],'name'=>$static_countries[$arrayCountry['countryname']]['cn_short_de'],'statiticstoday'=>array('confirmed'=>number_format($arrayCountry['confirmed'],0,",","."),'deaths'=>number_format($arrayCountry['deaths'],0,",","."),'recovered'=>number_format($arrayCountry['recovered'],0,",","."),'rfactor'=>$rFactor), 'staticsbefore'=>$beforeArray);
		}else{
			$dataArray =array('countryName'=>$arrayCountry['countryname'],'countryCode'=>$static_countries[$arrayCountry['countryname']]['cn_iso_2'],'name'=>$static_countries[$arrayCountry['countryname']]['cn_short_de'],'statiticstoday'=>array('confirmed'=>number_format($arrayCountry['confirmed'],0,",","."),'deaths'=>number_format($arrayCountry['deaths'],0,",","."),'recovered'=>number_format($arrayCountry['recovered'],0,",",".")), 'staticsbefore'=>$beforeArray);
		}

		

		$countryDetails [] = $dataArray;

		$confirmed +=$arrayCountry['confirmed'];
		$deaths    += $arrayCountry['deaths'];
		$recovered +=$arrayCountry['recovered'];

    }
		$row++;
		
    }
	fclose($handle);
	
	$beforeArrayWorld = array('confirmedBefore'=>number_format($confirmed-$beforeConfirmed,0,",","."),'deathsBefore'=>number_format($deaths-$beforeDeaths,0,",","."),'recoveredBefore'=>number_format($recovered-$beforeRecovered,0,",","."));
	$countryDetails [] = array('countryName'=>'Worldwide','countryCode'=>'WW','name'=>'Weltweit','statiticstoday'=>array('confirmed'=>number_format($confirmed,0,",","."),'deaths'=>number_format($deaths,0,",","."),'recovered'=>number_format($recovered,0,",",".")), 'staticsbefore'=>$beforeArrayWorld);
	
	// echo "<pre>";
	// print_r($countryDetails);
	// echo "<pre>";
	// die('test');

	$i=0;
	$j=1;
    if(!empty($countryDetails)){
		foreach ($countryDetails as $countryDetail){
			if($countryDetail['countryName']=='Worldwide'){
				$sortedArray[$i]=$countryDetail;
			}
			else{
				$sortedArray[$j]=$countryDetail;
				$j++;
			}
		}
		ksort($sortedArray);
		//json_encode($data,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $finaldata=json_encode(array(
            'status' => 200, // success or not?
            'message' =>'success',
			'countrydetails' => $sortedArray,
			
            ),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
			$date = date('Y-m-d H:i:s');
			$countrywisedata = mysqli_real_escape_string ($conn,$finaldata);
			$query = mysqli_query($conn, "SELECT * FROM `countrydetails`");

			if(mysqli_num_rows($query) > 0){
				//Update DB

				$sqldeletecountrywise ="DELETE FROM countrydetails";
				mysqli_query($conn,$sqldeletecountrywise);
				$sql_statement="INSERT INTO countrydetails (CountryData,  created_at,file_updated_time) VALUES ('$countrywisedata','$date','$date')";
				mysqli_query($conn,$sql_statement);
				

			}else{
				//INsert into DB
				$sql_statement="INSERT INTO countrydetails (CountryData,  created_at,file_updated_time) VALUES ('$countrywisedata','$date','$date')";
				mysqli_query($conn,$sql_statement);

			}
			
			
	
	
				
    }
  
	

		
 }


    }

    // if($timemodified > ){

    // }

}

else{

              
$date = date("Y-m-d");
$baseurl = "http://p546259.mittwaldserver.info/owncsv/";
$year = date("Y");
$month = date("m");
$char_slash = "/";
$basefilename = "2-COVIDCronjob-19-";
$extension = ".csv";
$date_before = date( 'Y-m-d', strtotime( $date . ' -1 day' ) );
$month_before = date( 'm', strtotime( $date . ' -1 day' ) );
$year_before = date( 'Y', strtotime( $date . ' -1 day' ) );

$csvName = $baseurl . $basefilename.$date.$extension;
$csvNameDayBefore = $baseurl . $basefilename . $date_before . $extension;

$row = 1;
$rowDayBefore = 1;
$arrayCountriesDaybefore = array();

//for getting data before today
if (($handleDaybefore = fopen($csvNameDayBefore, "r")) !== FALSE) {
    while (($dataDaybefore = fgetcsv($handleDaybefore, 1000, ";")) !== FALSE) {
	
	    if($rowDayBefore == 1 ){
			if($dataDaybefore[0] != 'Country'){
				die();	
			}
		}
		if($rowDayBefore != 1) {
			$arrayCountryDaybefore = array();
			$arrayCountryDaybefore['countryname']= $dataDaybefore[0];
			$arrayCountryDaybefore['confirmed']= $dataDaybefore[1];
			$arrayCountryDaybefore['deaths']= $dataDaybefore[2];
			$arrayCountryDaybefore['recovered']= $dataDaybefore[3];
			$arrayCountryDaybefore['latitude']= $dataDaybefore[4];
			$arrayCountryDaybefore['longitude']= $dataDaybefore[5];
			$arrayCountryDaybefore['countrycode']= $dataDaybefore[6];
			$arrayCountriesDaybefore[]= $arrayCountryDaybefore;
		}
		$rowDayBefore++;
	}
}

//for getting today's data
$beforeConfirmed=0;
$beforeDeaths=0;
$beforeRecovered =0;
$confirmed =0;
$deaths    = 0;
$recovered =0;


$arrayCountries = array();
if (($handle = fopen($csvName, "r")) !== FALSE) {
	
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
    if($row == 1 ){
      if($data[0] != 'Country'){
        die();	
      }
    }

    if($row != 1) {
      	$arrayCountry = array();
      	$arrayCountry['countryname']= $data[0];
      	$arrayCountry['confirmed']= $data[1];
        $arrayCountry['deaths']= $data[2];
        $arrayCountry['recovered']= $data[3];
      	// $arrayCountry['latitude']= $data[4];
      	// $arrayCountry['longitude']= $data[5];
		$arrayCountry['countrycode']= $data[6];
		
		foreach($arrayCountriesDaybefore as $before){
			if(($arrayCountry['countryname']==$before['countryname']) && ( $arrayCountry['countrycode']==$before['countrycode']) ){

				//$beforeArray = array('confirmedBefore'=>number_format($before['confirmed'],0,",","."),'deathsBefore'=>number_format($before['deaths'],0,",","."),'recoveredBefore'=>number_format($before['recovered'],0,",","."));
				$confirmedDayBeforeString = number_format(($arrayCountry['confirmed'] - $before['confirmed']),0,",",".");
				$deathsDayBeforeString    = number_format(($arrayCountry['deaths'] - $before['deaths']),0,",",".");
				$recovredDayBeforeString  = number_format(($arrayCountry['recovered'] - $before['recovered']),0,",",".");
				$beforeArray = array('confirmedBefore'=>$confirmedDayBeforeString,'deathsBefore'=>$deathsDayBeforeString,'recoveredBefore'=>$recovredDayBeforeString);
				$beforeConfirmed +=$before['confirmed'];
				$beforeDeaths +=$before['deaths'];
				$beforeRecovered +=$before['recovered'];

			}
		}
		$rFactornTrends=getTrendsnRfactor($static_countries[$arrayCountry['countryname']]['cn_iso_2']);

		
		
		if($rFactornTrends['rfactor']!=-1234 ){
			$rFactor = $rFactornTrends['rfactor'];
			$beforeArray['rfactor']=$rFactornTrends['trend'];
			$dataArray =array('countryName'=>$arrayCountry['countryname'],'countryCode'=>$static_countries[$arrayCountry['countryname']]['cn_iso_2'],'name'=>$static_countries[$arrayCountry['countryname']]['cn_short_de'],'statiticstoday'=>array('confirmed'=>number_format($arrayCountry['confirmed'],0,",","."),'deaths'=>number_format($arrayCountry['deaths'],0,",","."),'recovered'=>number_format($arrayCountry['recovered'],0,",","."),'rfactor'=>$rFactor), 'staticsbefore'=>$beforeArray);
		}else{
			$dataArray =array('countryName'=>$arrayCountry['countryname'],'countryCode'=>$static_countries[$arrayCountry['countryname']]['cn_iso_2'],'name'=>$static_countries[$arrayCountry['countryname']]['cn_short_de'],'statiticstoday'=>array('confirmed'=>number_format($arrayCountry['confirmed'],0,",","."),'deaths'=>number_format($arrayCountry['deaths'],0,",","."),'recovered'=>number_format($arrayCountry['recovered'],0,",",".")), 'staticsbefore'=>$beforeArray);
		}

		

		$countryDetails [] = $dataArray;

		$confirmed +=$arrayCountry['confirmed'];
		$deaths    += $arrayCountry['deaths'];
		$recovered +=$arrayCountry['recovered'];

    }
		$row++;
		
    }
	fclose($handle);
	
	$beforeArrayWorld = array('confirmedBefore'=>number_format($confirmed-$beforeConfirmed,0,",","."),'deathsBefore'=>number_format($deaths-$beforeDeaths,0,",","."),'recoveredBefore'=>number_format($recovered-$beforeRecovered,0,",","."));
	$countryDetails [] = array('countryName'=>'Worldwide','countryCode'=>'WW','name'=>'Weltweit','statiticstoday'=>array('confirmed'=>number_format($confirmed,0,",","."),'deaths'=>number_format($deaths,0,",","."),'recovered'=>number_format($recovered,0,",",".")), 'staticsbefore'=>$beforeArrayWorld);
	
	// echo "<pre>";
	// print_r($countryDetails);
	// echo "<pre>";
	// die('test');

	$i=0;
	$j=1;
    if(!empty($countryDetails)){
		foreach ($countryDetails as $countryDetail){
			if($countryDetail['countryName']=='Worldwide'){
				$sortedArray[$i]=$countryDetail;
			}
			else{
				$sortedArray[$j]=$countryDetail;
				$j++;
			}
		}
		ksort($sortedArray);
		//json_encode($data,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $finaldata=json_encode(array(
            'status' => 200, // success or not?
            'message' =>'success',
			'countrydetails' => $sortedArray,
			
            ),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
			$date = date('Y-m-d H:i:s');
			$countrywisedata = mysqli_real_escape_string ($conn,$finaldata);
			$query = mysqli_query($conn, "SELECT * FROM `countrydetails`");

			if(mysqli_num_rows($query) > 0){
				//Update DB

				$sqldeletecountrywise ="DELETE FROM countrydetails";
				mysqli_query($conn,$sqldeletecountrywise);
				$sql_statement="INSERT INTO countrydetails (CountryData,  created_at,file_updated_time) VALUES ('$countrywisedata','$date','$date')";
				mysqli_query($conn,$sql_statement);
				

			}else{
				//INsert into DB
				$sql_statement="INSERT INTO countrydetails (CountryData,  created_at,file_updated_time) VALUES ('$countrywisedata','$date','$date')";
				mysqli_query($conn,$sql_statement);

			}
			
			
	
	
				
    }
  
	

		
 }





 } 

    
   

}



function  getTrendsnRfactor($countrycode){

	
	require 'config.php';
	//$url = 'http://staging.hepsoft.com/php72/wordpress/?rest_route=/v1/reproduction_factor';
	$url = $ConFig['urlrfactor'];
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json'));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$data = curl_exec($curl);
	if($data===false){
		$return['rfactor']=-1234;
		$return['trend']=-1234;
		return $data;
	}
	else{
		$arrayDetails = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $data), true );
		foreach($arrayDetails['countrydetails'] as $country){

			if($country['countryCode']==$countrycode){
				$return['rfactor']=$country['rfactor'];
				$return['trend']=$country['trends'];
				return $return;

				
			}


		}

		
		$return['rfactor']=-1234;
		$return['trend']=-1234;
		return $return;

	}
    
   
	}

?>