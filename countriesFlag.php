<?php

require_once 'cronjobConfig.php';
$path = getcwd().'/flags/';
error_reporting(0);

foreach($static_countries as $key=> $countries){
    $svgFile=file_get_contents($path.$countries['cn_iso_2'].'.svg');
    if($svgFile){
        $countryName =$key;
        $String = base64_encode($svgFile); 
        $imgArray = array('countryname'=>$countryName, 'countryCode'=>$countries['cn_iso_2'],'imageString'=>$String);
        $countryFlags[] = $imgArray;
    

    }
    
}
if(!empty($countryFlags)){
  
	$finaldata=json_encode(array(
		'status' => 200, // success or not?
		'message' =>'success',
		'flagData' => $countryFlags,
		
		));
		header('Content-type: application/json');
		print_r( $finaldata);
		return $finaldata;
}
header('Content-type: application/json');
$finaldata=json_encode(array(
	'status' => 200, // success or not?
	'message' =>'failed',
	'flagData' => []
	
    ));
    print_r( $finaldata);
		return $finaldata;
?>