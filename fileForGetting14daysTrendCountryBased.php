<?php

require_once 'cronjobConfig.php';

$countryCode=isset($_GET['countrycode'])? strtoupper($_GET['countrycode']):"File#!123";


if($countryCode=="WW"){
	error_reporting(0);
	// Block for TravellerAssist
	$date = date("Y-m-d");
	$baseurl = "http://p546259.mittwaldserver.info/owncsv/";
	// $baseurl = "owncsv/";
	$year = date("Y");
	$month = date("m");
	$char_slash = "/";
	$basefilename = "2-COVIDCronjob-19-";
	$extension = ".csv";
	
	$date_before = date( 'Y-m-d', strtotime( $date . ' -1 day' ) );
	$month_before = date( 'm', strtotime( $date . ' -1 day' ) );
	$year_before = date( 'Y', strtotime( $date . ' -1 day' ) );
	
	$csvName = $baseurl . $basefilename . $date . $extension;
	$csvNameDayBefore = $baseurl . $basefilename . $date_before . $extension;
	
	$row = 1;
	$rowDayBefore = 1;
	$arrayCountriesDaybefore = array();
	
	
	
	if (($handleDaybefore = fopen($csvNameDayBefore, "r")) !== FALSE) {
		while (($dataDaybefore = fgetcsv($handleDaybefore, 1000, ";")) !== FALSE) {
		
			if($rowDayBefore == 1 ){
				if($dataDaybefore[0] != 'Country'){
					die();	
				}
			}
			
			if($rowDayBefore != 1) {
				$arrayCountryDaybefore = array();
				$arrayCountryDaybefore['countryname']= $dataDaybefore[0];
				$arrayCountryDaybefore['confirmed']= $dataDaybefore[1];
				$arrayCountryDaybefore['deaths']= $dataDaybefore[2];
				$arrayCountryDaybefore['recovered']= $dataDaybefore[3];
				$arrayCountryDaybefore['latitude']= $dataDaybefore[4];
				$arrayCountryDaybefore['longitude']= $dataDaybefore[5];
				$arrayCountryDaybefore['countrycode']= $dataDaybefore[6];
	
				
				
				$arrayCountriesDaybefore[]= $arrayCountryDaybefore;
			}
			$rowDayBefore++;
		}
	}
	
	//echo "<pre>".var_dump($arrayCountriesDaybefore)."</pre><br>";
	//print_r(array_values($arrayCountriesDaybefore));
	
	
	$arrayCountries = array();
	
	if (($handle = fopen($csvName, "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
	
			// if($row != 1){
			// 	$arrayCountry = array();
			// 	$arrayCountry['countryname']= $data[1];
			// 	$arrayCountry['provinz']= $data[0];
			// 	$arrayCountry['last_update']= $data[2];
			// 	$arrayCountry['confirmed']= $data[3];
			// 	$arrayCountry['deaths']= $data[4];
			// 	$arrayCountry['recovered']= $data[5];
			// 	$arrayCountry['latitude']= $data[6];
			// 	$arrayCountry['longitude']= $data[7];
			// 	$arrayCountries[]= $arrayCountry;
	
		// }
	
		if($row == 1 ){
		  if($data[0] != 'Country'){
			die();	
		  }
		}
	
		if($row != 1) {
		  $arrayCountry = array();
			  $arrayCountry['countryname']= $data[0];
			  $arrayCountry['confirmed']= $data[1];
			$arrayCountry['deaths']= $data[2];
			$arrayCountry['recovered']= $data[3];
			
			  $arrayCountry['latitude']= $data[4];
			  $arrayCountry['longitude']= $data[5];
			  $arrayCountry['countrycode']= $data[6];
			$deathspercent = 0;
			$confirmedpercent = 0;
			
			$arrayCountry['countrysize']= $data[7];
						
			if($data[2] !== 0){
				$deathspercent = (($data[2] / $data[7]) *100);
			}
			if($data[1] !== 0){
				$confirmedpercent = (($data[1] / $data[7]) * 100);
			}
			if($deathspercent > 0 && $deathspercent <= 0.01 ){
				$deathspercent = "<0,01";
			}
			
			if($deathspercent != "<0,01"){
					$deathspercent	=	number_format($deathspercent,2,",",".");
			}
			if($confirmedpercent > 0 && $confirmedpercent <= 0.01 ){
				$confirmedpercent = "<0,01";
			}
			
			if($confirmedpercent != "<0,01"){
					$confirmedpercent	=	number_format($confirmedpercent,2,",",".");
			}
			
			$deathspercent .= "%";
			$confirmedpercent .= "%";
						
			$arrayCountry['countryConfirmedPercent']= $confirmedpercent;
			$arrayCountry['countryDeathsPercent']= $deathspercent;
			
			
			
			
			
			  $arrayCountries[]= $arrayCountry;
		}
			$row++;
		}
		fclose($handle);
	  
		
		//CSV so bearbeiten das Sie für die App genutzt werden kann
		$arrayForApp = array();
		foreach($arrayCountries as $countries){
			
			// unset($countries['latitude']);
			// unset($countries['longitude']);
			$needTobeAdded = true;
			$indexArray = 0;
			$countryRecoveredDaybefore=0;
			$countryDeathsDaybefore=0;
			$countryRecoveredDaybefore=0;
			
			foreach($arrayCountriesDaybefore as $countriesDaybefore){
				if($countriesDaybefore['countryname'] == $countries['countryname']){
					$countryConfirmedDaybefore =  $countriesDaybefore['confirmed'];	
					$countryDeathsDaybefore =  $countriesDaybefore['deaths'];
					$countryRecoveredDaybefore =  $countriesDaybefore['recovered'];
				}
			}
			$countries['confirmedDayBefore'] = $countryConfirmedDaybefore;
			$countries['deathsDayBefore'] = $countryDeathsDaybefore;
			$countries['recoveredDayBefore'] = $countryRecoveredDaybefore;
			
			
			$deathsTrendpercent = 0;
			$confirmedTrendpercent = 0;
			if($countryDeathsDaybefore != 0 ){	
				
				$deathsTrendpercent = (($countries['deaths'] / $countryDeathsDaybefore) *100) -100;
			}
			if($countryConfirmedDaybefore != 0){
				
			$confirmedTrendpercent = (($countries['confirmed'] / $countryConfirmedDaybefore) * 100) -100;
			
				
				
			}
			
			$confirmedTrendpercentString = '';
			$deathsTrendpercentString = '';
			
			$countries['countryConfirmedTrendPercent']= $confirmedTrendpercent;
			$countries['countryDeathsTrendPercent']= $deathsTrendpercent;
	
			if($deathsTrendpercent > 0 && $deathsTrendpercent <= 0.01 ){
				$deathsTrendpercentString = "<0,01";
			}
			
			if($deathsTrendpercent != "<0,01"){
					$deathsTrendpercentString	=	number_format($deathsTrendpercent,2,",",".");
			}
			if($confirmedTrendpercent > 0 && $confirmedTrendpercent <= 0.01 ){
				$confirmedTrendpercentString = "<0,01";
			}
			
			if($confirmedTrendpercent != "<0,01"){
					$confirmedTrendpercentString	=	number_format($confirmedTrendpercent,2,",",".");
			}
			
			
			
			if(($deathsTrendpercent == "<0,01" || $deathsTrendpercent > 0) && $deathsTrendpercent != 0 ){
				$deathsTrendpercentString = "+".$deathsTrendpercentString;
			}
			
			if($deathsTrendpercent === 0){
				$deathsTrendpercentString = "+/-".$deathsTrendpercent;
			}
			
			
			
			if(($confirmedTrendpercent == "<0,01" || $confirmedTrendpercent > 0) && $confirmedTrendpercent !== 0){
				$confirmedTrendpercentString = "+".$confirmedTrendpercentString;
			}
			
			if($confirmedTrendpercent === 0){
				$confirmedTrendpercentString = "+/-".$confirmedTrendpercent;
			}
			$deathsTrendpercentString .= "%";
			$confirmedTrendpercentString .= "%";
						
	
			$countries['countryConfirmedTrendPercentString']= $confirmedTrendpercentString;
			$countries['countryDeathsTrendPercentString']= $deathsTrendpercentString;
		
			
			$countryDifferentdeathsDaybefore =0;
			$countryDifferentconfirmedDaybefore = 0;
	
			
			$countryDifferentconfirmedDaybefore = number_format($countries['confirmed'] - $countries['confirmedDayBefore'] ,0,",",".");
			
			$countryDifferentdeathsDaybefore = number_format($countries['deaths'] - $countries['deathsDayBefore'] ,0,",",".");
			
			if($countryDifferentdeathsDaybefore > 0){
				$countryDifferentdeathsDaybefore = "+".$countryDifferentdeathsDaybefore;
			}
			
			if($countryDifferentconfirmedDaybefore > 0){
				$countryDifferentconfirmedDaybefore = "+".$countryDifferentconfirmedDaybefore;
			}
			$countries['deathsDifferentfromDaybefore'] = $countryDifferentdeathsDaybefore;
			$countries['confirmedDifferentfromDaybefore'] = $countryDifferentconfirmedDaybefore ;
		
		
			
			foreach($arrayForApp as $arrayApp){
				
	
				if($arrayApp['countryname'] == $countries['countryname']){
								
					$arrayApp['confirmed'] +=  $countries['confirmed'];				
					$arrayApp['deaths'] +=  $countries['deaths'];					
					$arrayApp['recovered'] +=  $countries['recovered'];		
					
					$arrayApp['confirmedDayBefore'] =  $countries['confirmedDayBefore'];
					$arrayApp['deathsDayBefore'] =  $countries['deathsDayBefore'];
					$arrayApp['recoveredDayBefore'] =  $countries['recoveredDayBefore'];
					
					
					
	
					
					
					$arrayForApp[$indexArray] = $arrayApp;
					$needTobeAdded = false;
				}
				
				$indexArray ++;
			}
			if($needTobeAdded){
				$arrayForApp[]= $countries;
			}
	
		}
	
		
		$globalAppArray = array();
		$globalAppArray['countryname'] = "Worldwide";
		$globalAppArray['countrycode'] =  $static_countries[$globalAppArray['countryname']]['cn_iso_2'];	
		foreach($arrayForApp as $arrayApp){
			
			$globalAppArray['confirmed'] += $arrayApp['confirmed'];
			$globalAppArray['deaths'] += $arrayApp['deaths'];
			$globalAppArray['recovered'] += $arrayApp['recovered'];
			
			$globalAppArray['confirmedDayBefore'] += $arrayApp['confirmedDayBefore'];
			$globalAppArray['deathsDayBefore'] += $arrayApp['deathsDayBefore'];
			$globalAppArray['recoveredDayBefore'] += $arrayApp['recoveredDayBefore'];
			
			$globalAppArray['countrysize'] += $arrayApp['countrysize'];
			
		}
		$globalAppArray['confirmed'] = $letterQuats.$globalAppArray['confirmed'].$letterQuats;
		$globalAppArray['deaths'] = $letterQuats.$globalAppArray['deaths'].$letterQuats;
		$globalAppArray['recovered'] = $letterQuats.$globalAppArray['recovered'].$letterQuats;
		$globalAppArray['confirmedDayBefore'] = $letterQuats.$globalAppArray['confirmedDayBefore'].$letterQuats;
		$globalAppArray['deathsDayBefore'] = $letterQuats.$globalAppArray['deathsDayBefore'].$letterQuats;
		$globalAppArray['recoveredDayBefore'] = $letterQuats.$globalAppArray['recoveredDayBefore'].$letterQuats;
		$globalAppArray['countrysize'] = $letterQuats.$globalAppArray['countrysize'].$letterQuats;
		
		
			$deathspercent = 0;
			$confirmedpercent = 0;
		
			if($globalAppArray['deaths']!== 0){
				$deathspercent = (($globalAppArray['deaths'] / $globalAppArray['countrysize'] ) *100);
			}
			if($globalAppArray['confirmed'] !== 0){
				$confirmedpercent = (($globalAppArray['confirmed'] / $globalAppArray['countrysize'] ) * 100);
			}
			if($deathspercent > 0 && $deathspercent <= 0.01 ){
				$deathspercent = "<0,01";
			}
			
			if($deathspercent != "<0,01"){
					$deathspercent	=	number_format($deathspercent,2,",",".");
			}
			if($confirmedpercent > 0 && $confirmedpercent <= 0.01 ){
				$confirmedpercent = "<0,01";
			}
			
			if($confirmedpercent != "<0,01"){
					$confirmedpercent	=	number_format($confirmedpercent,2,",",".");
			}
			
			$deathspercent .= "%";
			$confirmedpercent .= "%";
						
			$globalAppArray['countryConfirmedPercent']= $confirmedpercent;
			$globalAppArray['countryDeathsPercent']= $deathspercent;
			
			$globalDifferentdeathsDaybefore =0;
			$globalDifferentconfirmedDaybefore = 0;
			
			$globalDifferentconfirmedDaybefore = number_format($globalAppArray['confirmed'] - $globalAppArray['confirmedDayBefore'] ,0,",",".");
			
			$globalDifferentdeathsDaybefore = number_format($globalAppArray['deaths'] - $globalAppArray['deathsDayBefore'] ,0,",",".");
			
			if($globalDifferentdeathsDaybefore > 0){
				$globalDifferentdeathsDaybefore = "+".$globalDifferentdeathsDaybefore;
			}
			
			if($globalDifferentconfirmedDaybefore > 0){
				$globalDifferentconfirmedDaybefore = "+".$globalDifferentconfirmedDaybefore;
			}
			$globalAppArray['deathsDifferentfromDaybefore'] = $globalDifferentdeathsDaybefore;
			$globalAppArray['confirmedDifferentfromDaybefore'] = $globalDifferentconfirmedDaybefore ;
		
		
			$deathsTrendpercent = 0;
			$confirmedTrendpercent = 0;
			if($globalAppArray['deathsDayBefore'] != 0 ){	
				$deathsTrendpercent = (($globalAppArray['deaths'] / $globalAppArray['deathsDayBefore']) *100) -100;
			}
			if($globalAppArray['confirmedDayBefore'] != 0){
			$confirmedTrendpercent = (($globalAppArray['confirmed'] / $globalAppArray['confirmedDayBefore']) * 100) -100;
			
				
				
			}
			
			$confirmedTrendpercentString = '';
			$deathsTrendpercentString = '';
			
			$globalAppArray['countryConfirmedTrendPercent']= $confirmedTrendpercent;
			$globalAppArray['countryDeathsTrendPercent']= $deathsTrendpercent;
	
			if($deathsTrendpercent > 0 && $deathsTrendpercent <= 0.01 ){
				$deathsTrendpercentString = "<0,01";
			}
			
			if($deathsTrendpercent != "<0,01"){
					$deathsTrendpercentString	=	number_format($deathsTrendpercent,2,",",".");
			}
			if($confirmedTrendpercent > 0 && $confirmedTrendpercent <= 0.01 ){
				$confirmedTrendpercent = "<0,01";
			}
			
			if($confirmedTrendpercent != "<0,01"){
					$confirmedTrendpercentString	=	number_format($confirmedTrendpercent,2,",",".");
			}
			
			
			
			if(($deathsTrendpercent == "<0,01" || $deathsTrendpercent > 0) && $deathsTrendpercent != 0 ){
				$deathsTrendpercentString = "+".$deathsTrendpercentString;
			}
			
			if($deathsTrendpercent === 0){
				$deathsTrendpercentString = "+/-".$deathsTrendpercent;
			}
			
			if(($confirmedTrendpercent == "<0,01" || $confirmedTrendpercent > 0)){
				$confirmedTrendpercentString = "+".$confirmedTrendpercentString;
			}
			
			if($confirmedTrendpercent === 0){
				$confirmedTrendpercentString = "+/-".$confirmedTrendpercentString;
			}
			$deathsTrendpercentString .= "%";
			$confirmedTrendpercentString .= "%";
						
	
			$globalAppArray['countryConfirmedTrendPercentString']= $confirmedTrendpercentString;
			$globalAppArray['countryDeathsTrendPercentString']= $deathsTrendpercentString;
			
		
		
		$indexArrayUebersetzung = 0;
	
		
		$indexArrayUebersetzungGlobal = 0;			
		if(array_key_exists($globalAppArray['countryname'],$static_countries)){
		
			$globalAppArray['name'] =  $static_countries[$globalAppArray['countryname']]['cn_short_de'];				
			//$globalAppArray[$indexArrayUebersetzungGlobal] = $globalAppArray;
		}
	
		$arrayForAppFinal = array();
		$arrayForAppFinal[]= $globalAppArray;
		
		$givenbackArray [] = $globalAppArray;
	
		
		
		
		$arrayCountries14Days [] = $arrayForAppFinal;
	
		for($intI = 14; $intI > 0;$intI--){
			$date_before = date( 'Y-m-d', strtotime( $date . ' -'.$intI.' day' ) );
			$year_before = date( 'Y', strtotime( $date . ' -'.$intI.' day' ) );
			$month_before = date( 'm', strtotime( $date . ' -'.$intI.' day' ) );
		
			$csvName14DaysBefore = $baseurl .$basefilename . $date_before . $extension;
			
			$row14Daysbefore = 1;
			
			$arrayCountries = array();
			if (($handle14DaysBefore = fopen($csvName14DaysBefore, "r")) !== FALSE) {
			
				while (($data14DaysBefore = fgetcsv($handle14DaysBefore, 1000, ";")) !== FALSE) {
					
					if($row14Daysbefore == 1 ){
	
						$isRightCSVCorrection = true;
						if($data14DaysBefore[0] != 'Country'){
							$isRightCSVCorrection = false;
						}
					  
						if($data14DaysBefore[1] != 'Confirmed Cases'){
							$isRightCSVCorrection = false;	
						}
						  
						if($data14DaysBefore[2] != 'Deaths'){
							$isRightCSVCorrection = false;	
						}
						  
						if($data14DaysBefore[3] != 'Recovered'){
							$isRightCSVCorrection = false;
						}
						if($data14DaysBefore[4] != 'Latitude'){
							$isRightCSVCorrection = false;	
						}
						if($data14DaysBefore[5] != 'Longitude'){
							$isRightCSVCorrection = false;	
						}
						  
						if($data14DaysBefore[6] != 'Country Code'){
							$isRightCSVCorrection = false;
						}
					  
					//echo $csvName14DaysBefore."<br>";
					}
	
					
					if($isRightCSVCorrection){
						if($row14Daysbefore != 1) {
							$arrayCountry14DaysBefore = array();
							//if($data14DaysBefore[6]=='WW'){
							$arrayCountry14DaysBefore['countryname']= $data14DaysBefore[0];
							$arrayCountry14DaysBefore['name']= $static_countries[$data14DaysBefore[0]]['cn_short_de'];
							$arrayCountry14DaysBefore['confirmed']= $data14DaysBefore[1];
							$arrayCountry14DaysBefore['deaths']= $data14DaysBefore[2];
							$arrayCountry14DaysBefore['recovered']= $data14DaysBefore[3];
							//$arrayCountry14DaysBefore['countrycode']= $data14DaysBefore[6];
							
							$arrayCountries14[$intI][]= $arrayCountry14DaysBefore;
							//}
						}
					}
					$row14Daysbefore++;
						
						
				}
				fclose($handle14DaysBefore);
				
				
				
				
				$globalAppArray = array();
				$globalAppArray['countryname'] = "Worldwide";
				$globalAppArray['name'] = "Weltweit";
				foreach($arrayCountries14[$intI] as $arrayApp){
					$globalAppArray['confirmed'] += $arrayApp['confirmed'];
					$globalAppArray['deaths'] += $arrayApp['deaths'];
					$globalAppArray['recovered'] += $arrayApp['recovered'];
	
				}
				$globalAppArray['confirmed'] =  $letterQuats.$globalAppArray['confirmed'].$letterQuats;
				$globalAppArray['deaths'] = $letterQuats.$globalAppArray['deaths'].$letterQuats;
				$globalAppArray['recovered'] = $letterQuats.$globalAppArray['recovered'].$letterQuats;
				$globalAppArray['countrycode'] =  $static_countries[$globalAppArray['countryname']]['cn_iso_2'];	
				
				
	
				$arrayForAppFinal = array();
				$arrayForAppFinal[]= $globalAppArray;
				
			
			
				//Array in CSV verwandeln oder jegliche gewünschte Format
				foreach($arrayCountries as $arrayAppFinal){
					
					if($arrayAppFinal['countryname'] == "Worldwide" ){
						$arrayForAppFinal[] = $arrayAppFinal;
					}
				}
				
				
				
				
				 if($isRightCSVCorrection){
					$trendsof14days[] =  $arrayForAppFinal[0];
				}
					
	
				
			}
			
			
			
		}
		
	
		if(!empty($givenbackArray)){
			$givenbackArray[0]['confirmed']=number_format($givenbackArray[0]['confirmed'],0,",",".");
			$givenbackArray[0]['deaths']=number_format($givenbackArray[0]['deaths'],0,",",".");
			$givenbackArray[0]['recovered']=number_format($givenbackArray[0]['recovered'],0,",",".");
			
			$finaldata=json_encode(array(
				'status' => 200, // success or not?
				'message' =>'success',
				'countrydetails' => $givenbackArray,
				'trends'=>$trendsof14days
				),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
		
				header('Content-type: application/json');
				print_r( $finaldata);
				return $finaldata;
		}
		header('Content-type: application/json');
		$finaldata=json_encode(array(
			'status' => 200, // success or not?
			'message' =>'failed',
			'countrydetails' => [],
			'trends'=>[]
			));
	
	
			print_r( $finaldata);
				
	}
}




$date = date("Y-m-d");
$baseurl = "http://p546259.mittwaldserver.info/owncsv/";
// $baseurl = "owncsv/";
$year = date("Y");
$month = date("m");
$char_slash = "/";
$basefilename = "2-COVIDCronjob-19-";
$extension = ".csv";

$date_before = date( 'Y-m-d', strtotime( $date . ' -1 day' ) );
$month_before = date( 'm', strtotime( $date . ' -1 day' ) );
$year_before = date( 'Y', strtotime( $date . ' -1 day' ) );

$csvName = $baseurl . $basefilename . $date . $extension;
$csvNameDayBefore = $baseurl . $basefilename . $date_before . $extension;

$row = 1;
$rowDayBefore = 1;
$arrayCountriesDaybefore = array();



if (($handleDaybefore = fopen($csvNameDayBefore, "r")) !== FALSE) {
    while (($dataDaybefore = fgetcsv($handleDaybefore, 1000, ";")) !== FALSE) {
	
	    if($rowDayBefore == 1 ){
			if($dataDaybefore[0] != 'Country'){
				die();	
			}
		}
		
		if($rowDayBefore != 1) {
			$arrayCountryDaybefore = array();
			$arrayCountryDaybefore['countryname']= $dataDaybefore[0];
			$arrayCountryDaybefore['confirmed']= $dataDaybefore[1];
			$arrayCountryDaybefore['deaths']= $dataDaybefore[2];
			$arrayCountryDaybefore['recovered']= $dataDaybefore[3];
			$arrayCountryDaybefore['latitude']= $dataDaybefore[4];
			$arrayCountryDaybefore['longitude']= $dataDaybefore[5];
			$arrayCountryDaybefore['countrycode']= $dataDaybefore[6];

			
			
			$arrayCountriesDaybefore[]= $arrayCountryDaybefore;
		}
		$rowDayBefore++;
	}
}

//echo "<pre>".var_dump($arrayCountriesDaybefore)."</pre><br>";
//print_r(array_values($arrayCountriesDaybefore));


$arrayCountries = array();

if (($handle = fopen($csvName, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

    if($row == 1 ){
      if($data[0] != 'Country'){
        die();	
      }
    }

    if($row != 1) {
        
     if($data[6]==$countryCode){
        $arrayCountry = array();
      	$arrayCountry['countryname']= $data[0];
      	$arrayCountry['confirmed']= $data[1];
        $arrayCountry['deaths']= $data[2];
        $arrayCountry['recovered']= $data[3];
      	$arrayCountry['latitude']= $data[4];
      	$arrayCountry['longitude']= $data[5];
      	$arrayCountry['countrycode']= $data[6];
		$deathspercent = 0;
		$confirmedpercent = 0;
		
		$arrayCountry['countrysize']= $data[7];
			
           	
        	
		if($data[2] !== 0){
			$deathspercent = (($data[2] / $data[7]) *100);
        }
    
		if($data[1] !== 0){
			$confirmedpercent = (($data[1] / $data[7]) * 100);
		}
		if($deathspercent > 0 && $deathspercent <= 0.01 ){
			$deathspercent = "<0,01";
		}
		
		if($deathspercent != "<0,01"){
				$deathspercent	=	number_format($deathspercent,2,",",".");
		}
		if($confirmedpercent > 0 && $confirmedpercent <= 0.01 ){
			$confirmedpercent = "<0,01";
		}
		
		if($confirmedpercent != "<0,01"){
				$confirmedpercent	=	number_format($confirmedpercent,2,",",".");
		}
		
		$deathspercent .= "%";
		$confirmedpercent .= "%";
					
		$arrayCountry['countryConfirmedPercent']= $confirmedpercent;
		$arrayCountry['countryDeathsPercent']= $deathspercent;
		
		
		
		
		
          $arrayCountries[]= $arrayCountry;
    }
    }
		$row++;
    }
//}
	fclose($handle);
	

	
	//CSV so bearbeiten das Sie für die App genutzt werden kann
	$arrayForApp = array();
	foreach($arrayCountries as $countries){
		
		// unset($countries['latitude']);
		// unset($countries['longitude']);
		$needTobeAdded = true;
		$indexArray = 0;
		$countryRecoveredDaybefore=0;
		$countryDeathsDaybefore=0;
		$countryRecoveredDaybefore=0;
		
		foreach($arrayCountriesDaybefore as $countriesDaybefore){
			if($countriesDaybefore['countryname'] == $countries['countryname']){
				$countryConfirmedDaybefore =  $countriesDaybefore['confirmed'];	
				$countryDeathsDaybefore =  $countriesDaybefore['deaths'];
				$countryRecoveredDaybefore =  $countriesDaybefore['recovered'];
			}
		}
		$countries['confirmedDayBefore'] = $countryConfirmedDaybefore;
		$countries['deathsDayBefore'] = $countryDeathsDaybefore;
		$countries['recoveredDayBefore'] = $countryRecoveredDaybefore;
		
		
		$deathsTrendpercent = 0;
		$confirmedTrendpercent = 0;
		if($countryDeathsDaybefore != 0 ){	
			$deathsTrendpercent = (($countries['deaths'] / $countryDeathsDaybefore) *100) -100;
		}
		if($countryConfirmedDaybefore != 0){
		$confirmedTrendpercent = (($countries['confirmed'] / $countryConfirmedDaybefore) * 100) -100;
		
			
			
		}
		
		$confirmedTrendpercentString = '';
		$deathsTrendpercentString = '';
		
		$countries['countryConfirmedTrendPercent']= $confirmedTrendpercent;
		$countries['countryDeathsTrendPercent']= $deathsTrendpercent;

		if($deathsTrendpercent > 0 && $deathsTrendpercent <= 0.01 ){
			$deathsTrendpercentString = "<0,01";
		}
		
		if($deathsTrendpercent != "<0,01"){
				$deathsTrendpercentString	=	number_format($deathsTrendpercent,2,",",".");
		}
		if($confirmedTrendpercent > 0 && $confirmedTrendpercent <= 0.01 ){
			$confirmedTrendpercentString = "<0,01";
		}
		
		if($confirmedTrendpercent != "<0,01"){
				$confirmedTrendpercentString	=	number_format($confirmedTrendpercent,2,",",".");
		}
		
		
		
		if(($deathsTrendpercent == "<0,01" || $deathsTrendpercent > 0) && $deathsTrendpercent != 0 ){
			$deathsTrendpercentString = "+".$deathsTrendpercentString;
		}
		
		if($deathsTrendpercent === 0){
			$deathsTrendpercentString = "+/-".$deathsTrendpercent;
		}
		
		
		
		if(($confirmedTrendpercent == "<0,01" || $confirmedTrendpercent > 0) && $confirmedTrendpercent !== 0){
			$confirmedTrendpercentString = "+".$confirmedTrendpercentString;
		}
		
		if($confirmedTrendpercent === 0){
			$confirmedTrendpercentString = "+/-".$confirmedTrendpercent;
		}
		$deathsTrendpercentString .= "%";
		$confirmedTrendpercentString .= "%";
					

		$countries['countryConfirmedTrendPercentString']= $confirmedTrendpercentString;
		$countries['countryDeathsTrendPercentString']= $deathsTrendpercentString;
	
		
		$countryDifferentdeathsDaybefore =0;
		$countryDifferentconfirmedDaybefore = 0;
		
		$countryDifferentconfirmedDaybefore = number_format($countries['confirmed'] - $countries['confirmedDayBefore'] ,0,",",".");
		
		$countryDifferentdeathsDaybefore = number_format($countries['deaths'] - $countries['deathsDayBefore'] ,0,",",".");
		
		if($countryDifferentdeathsDaybefore > 0){
			$countryDifferentdeathsDaybefore = "+".$countryDifferentdeathsDaybefore;
		}
		
		if($countryDifferentconfirmedDaybefore > 0){
			$countryDifferentconfirmedDaybefore = "+".$countryDifferentconfirmedDaybefore;
		}
		$countries['deathsDifferentfromDaybefore'] = $countryDifferentdeathsDaybefore;
		$countries['confirmedDifferentfromDaybefore'] = $countryDifferentconfirmedDaybefore ;
	
	
		
		foreach($arrayForApp as $arrayApp){
			

			if($arrayApp['countryname'] == $countries['countryname']){
							
				$arrayApp['confirmed'] +=  $countries['confirmed'];				
				$arrayApp['deaths'] +=  $countries['deaths'];					
				$arrayApp['recovered'] +=  $countries['recovered'];		
				
				$arrayApp['confirmedDayBefore'] =  $countries['confirmedDayBefore'];
				$arrayApp['deathsDayBefore'] =  $countries['deathsDayBefore'];
				$arrayApp['recoveredDayBefore'] =  $countries['recoveredDayBefore'];
				
				
				

				
				
				$arrayForApp[$indexArray] = $arrayApp;
				$needTobeAdded = false;
			}
			
			$indexArray ++;
		}
		if($needTobeAdded){
			$arrayForApp[]= $countries;
		}
		//$arrayForApp[0]['countryFlag'] = "http://virus.health-assist.de/flags/".$countryCode.".svg";
		$arrayForApp[0]['countryFlag'] = "http://staging.hepsoft.com/VirusResponse/flags/".$countryCode.".svg";
		
	}


		
		
		
	
	 $indexArrayUebersetzung = 0;
	foreach($arrayForApp as $arrayApp){
		
		
		if(array_key_exists($arrayApp['countryname'],$static_countries)){
			$arrayApp['name'] =  $static_countries[$arrayApp['countryname']]['cn_short_de'];				
			
			$arrayForApp[$indexArrayUebersetzung] = $arrayApp;
		}
		$indexArrayUebersetzung++;
	}
	


	

	
	

	$arrayCountries14Days = array();
	
	
	for($intI = 14; $intI > 0;$intI--){
		$date_before = date( 'Y-m-d', strtotime( $date . ' -'.$intI.' day' ) );
		$year_before = date( 'Y', strtotime( $date . ' -'.$intI.' day' ) );
		$month_before = date( 'm', strtotime( $date . ' -'.$intI.' day' ) );
	
		$csvName14DaysBefore = $baseurl .$basefilename . $date_before . $extension;
		
		$row14Daysbefore = 1;
		
		$arrayCountries = array();
		if (($handle14DaysBefore = fopen($csvName14DaysBefore, "r")) !== FALSE) {
		
			while (($data14DaysBefore = fgetcsv($handle14DaysBefore, 1000, ";")) !== FALSE) {
				
				if($row14Daysbefore == 1 ){

					$isRightCSVCorrection = true;
					if($data14DaysBefore[0] != 'Country'){
						$isRightCSVCorrection = false;
					}
				  
					if($data14DaysBefore[1] != 'Confirmed Cases'){
						$isRightCSVCorrection = false;	
					}
					  
					if($data14DaysBefore[2] != 'Deaths'){
						$isRightCSVCorrection = false;	
					}
					  
					if($data14DaysBefore[3] != 'Recovered'){
						$isRightCSVCorrection = false;
					}
					if($data14DaysBefore[4] != 'Latitude'){
						$isRightCSVCorrection = false;	
					}
					if($data14DaysBefore[5] != 'Longitude'){
						$isRightCSVCorrection = false;	
					}
					  
					if($data14DaysBefore[6] != 'Country Code'){
						$isRightCSVCorrection = false;
					}
				  
				//echo $csvName14DaysBefore."<br>";
				}

				
				if($isRightCSVCorrection){
					if($row14Daysbefore != 1) {
                       
                        $arrayCountry14DaysBefore = array();
						if($data14DaysBefore[6]==$countryCode){
							$arrayCountry14DaysBefore['countryname']= $data14DaysBefore[0];
							$arrayCountry14DaysBefore['name']= $static_countries[$data14DaysBefore[0]]['cn_short_de'];
							$arrayCountry14DaysBefore['confirmed']= $data14DaysBefore[1];
							$arrayCountry14DaysBefore['deaths']= $data14DaysBefore[2];
							$arrayCountry14DaysBefore['recovered']=  $data14DaysBefore[3];
							//$arrayCountry14DaysBefore['countrycode']= $data14DaysBefore[6];
							$arrayCountries[]= $arrayCountry14DaysBefore;
							}
					}
				}
				$row14Daysbefore++;
					
					
			}
			fclose($handle14DaysBefore);

			
			
			//Array in CSV verwandeln oder jegliche gewünschte Format
			foreach($arrayCountries as $arrayAppFinal){
				
				if($arrayAppFinal['countryname'] != "Worldwide" && isset($arrayAppFinal['countryname'])){
					$arrayForAppFinal[] = $arrayAppFinal;
				}
			}
			
			
			
			
			// if($isRightCSVCorrection){
			// 	$arrayCountries14Days[] =  $arrayForAppFinal;
			// }
				

			
		}
		
		
		
	}
	if(!empty($arrayForApp)){
		
		
		$arrayForApp[0]['confirmed']=number_format($arrayForApp[0]['confirmed'],0,",",".");
		$arrayForApp[0]['deaths']=number_format($arrayForApp[0]['deaths'],0,",",".");
		$arrayForApp[0]['recovered']=number_format($arrayForApp[0]['recovered'],0,",",".");
        $finaldata=json_encode(array(
            'status' => 200, // success or not?
            'message' =>'success',
			'countrydetails' => $arrayForApp,
			'trends'=>$arrayForAppFinal
            ),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
            header('Content-type: application/json');
            print_r( $finaldata);
            return $finaldata;
    }
    header('Content-type: application/json');
    $finaldata=json_encode(array(
        'status' => 200, // success or not?
        'message' =>'failed',
		'countrydetails' => [],
		'trends'=>[]
        ));


        print_r( $finaldata);
            return $finaldata;
	
	
	
	
	
	
		
}



?>