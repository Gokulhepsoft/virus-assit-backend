<?php

require_once 'cronjobConfig.php';

$date = date("Y-m-d");
$baseurl = "http://p546259.mittwaldserver.info/owncsv/";
$year = date("Y");
$month = date("m");
$char_slash = "/";
$basefilename = "2-COVIDCronjob-19-";
$extension = ".csv";
$date_before = date( 'Y-m-d', strtotime( $date . ' -1 day' ) );
$month_before = date( 'm', strtotime( $date . ' -1 day' ) );
$year_before = date( 'Y', strtotime( $date . ' -1 day' ) );

$csvName = $baseurl . $basefilename.$date.$extension;
$csvNameDayBefore = $baseurl . $basefilename . $date_before . $extension;

$row = 1;
$rowDayBefore = 1;
$arrayCountriesDaybefore = array();

//for getting data before today
if (($handleDaybefore = fopen($csvNameDayBefore, "r")) !== FALSE) {
    while (($dataDaybefore = fgetcsv($handleDaybefore, 1000, ";")) !== FALSE) {
	
	    if($rowDayBefore == 1 ){
			if($dataDaybefore[0] != 'Country'){
				die();	
			}
		}
		if($rowDayBefore != 1) {
			$arrayCountryDaybefore = array();
			$arrayCountryDaybefore['countryname']= $dataDaybefore[0];
			$arrayCountryDaybefore['confirmed']= $dataDaybefore[1];
			$arrayCountryDaybefore['deaths']= $dataDaybefore[2];
			$arrayCountryDaybefore['recovered']= $dataDaybefore[3];
			$arrayCountryDaybefore['latitude']= $dataDaybefore[4];
			$arrayCountryDaybefore['longitude']= $dataDaybefore[5];
			$arrayCountryDaybefore['countrycode']= $dataDaybefore[6];
			$arrayCountriesDaybefore[]= $arrayCountryDaybefore;
		}
		$rowDayBefore++;
	}
}

//for getting today's data
$beforeConfirmed=0;
$beforeDeaths=0;
$beforeRecovered =0;
$confirmed =0;
$deaths    = 0;
$recovered =0;


$arrayCountries = array();
if (($handle = fopen($csvName, "r")) !== FALSE) {
	
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
    if($row == 1 ){
      if($data[0] != 'Country'){
        die();	
      }
    }

    if($row != 1) {
      	$arrayCountry = array();
      	$arrayCountry['countryname']= $data[0];
      	$arrayCountry['confirmed']= $data[1];
        $arrayCountry['deaths']= $data[2];
        $arrayCountry['recovered']= $data[3];
      	// $arrayCountry['latitude']= $data[4];
      	// $arrayCountry['longitude']= $data[5];
		$arrayCountry['countrycode']= $data[6];
		
		foreach($arrayCountriesDaybefore as $before){
			if(($arrayCountry['countryname']==$before['countryname']) && ( $arrayCountry['countrycode']==$before['countrycode']) ){

				//$beforeArray = array('confirmedBefore'=>number_format($before['confirmed'],0,",","."),'deathsBefore'=>number_format($before['deaths'],0,",","."),'recoveredBefore'=>number_format($before['recovered'],0,",","."));
				$confirmedDayBeforeString = number_format(($arrayCountry['confirmed'] - $before['confirmed']),0,",",".");
				$deathsDayBeforeString    = number_format(($arrayCountry['deaths'] - $before['deaths']),0,",",".");
				$recovredDayBeforeString  = number_format(($arrayCountry['recovered'] - $before['recovered']),0,",",".");
				$beforeArray = array('confirmedBefore'=>$confirmedDayBeforeString,'deathsBefore'=>$deathsDayBeforeString,'recoveredBefore'=>$recovredDayBeforeString);
				$beforeConfirmed +=$before['confirmed'];
				$beforeDeaths +=$before['deaths'];
				$beforeRecovered +=$before['recovered'];

			}
		}
		
		$dataArray =array('countryName'=>$arrayCountry['countryname'],'countryCode'=>$static_countries[$arrayCountry['countryname']]['cn_iso_2'],'name'=>$static_countries[$arrayCountry['countryname']]['cn_short_de'],'statiticstoday'=>array('confirmed'=>number_format($arrayCountry['confirmed'],0,",","."),'deaths'=>number_format($arrayCountry['deaths'],0,",","."),'recovered'=>number_format($arrayCountry['recovered'],0,",",".")), 'staticsbefore'=>$beforeArray);

		$countryDetails [] = $dataArray;

		$confirmed +=$arrayCountry['confirmed'];
		$deaths    += $arrayCountry['deaths'];
		$recovered +=$arrayCountry['recovered'];

    }
		$row++;
		
    }
	fclose($handle);
	$beforeArrayWorld = array('confirmedBefore'=>number_format($confirmed-$beforeConfirmed,0,",","."),'deathsBefore'=>number_format($deaths-$beforeDeaths,0,",","."),'recoveredBefore'=>number_format($recovered-$beforeRecovered,0,",","."));
	$countryDetails [] = array('countryName'=>'Worldwide','countryCode'=>'WW','name'=>'Weltweit','statiticstoday'=>array('confirmed'=>number_format($confirmed,0,",","."),'deaths'=>number_format($deaths,0,",","."),'recovered'=>number_format($recovered,0,",",".")), 'staticsbefore'=>$beforeArrayWorld);
	
	// echo "<pre>";
	// print_r($countryDetails);
	// echo "<pre>";
	// die('test');

	$i=0;
	$j=1;
    if(!empty($countryDetails)){
		foreach ($countryDetails as $countryDetail){
			if($countryDetail['countryName']=='Worldwide'){
				$sortedArray[$i]=$countryDetail;
			}
			else{
				$sortedArray[$j]=$countryDetail;
				$j++;
			}
		}
		ksort($sortedArray);
		//json_encode($data,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $finaldata=json_encode(array(
            'status' => 200, // success or not?
            'message' =>'success',
			'countrydetails' => $sortedArray,
			
            ),JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
            header('Content-type: application/json');
            print_r( $finaldata);
            return $finaldata;
    }
    header('Content-type: application/json');
    return json_encode(array(
        'status' => 200, // success or not?
        'message' =>'failed',
		'countrydetails' => [],
		
        ));
	

		
}



?>